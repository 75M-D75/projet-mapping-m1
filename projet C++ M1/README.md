
### Description

Genere des fichiers alignement.json position.as et html pour visualiser l'alignemeent, voir dans les dossier alignement, html, positions aprés execution

## Execution de test
    make rm_o
    make test
    ./bin/test chemin_ficher_fasta_ou_fastq [read_File]
    
## Execution du programme
    make rm_o
    make
    ./bin/main chemin_ficher_fasta_ou_fastq read_file
    
## Execution simplifier
    make rm_o
    make
    make run
  
# Exemple
    ./bin/main data/run7.fastq data/fi.fastq
    ./bin/test data/run7.fastq
    ./bin/test data/run7.fastq data/fi.fastq
    ./bin/test data/fi.fastq
