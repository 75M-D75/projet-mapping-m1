#ifndef DEF_FICHIER_FAST_X
#define DEF_FICHIER_FAST_X
#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fstream>
#include <vector>
#include <regex>


#include "SequenceFastX.h"
#include "SequenceFasta.h"
#include "SequenceFastq.h"



using namespace std;

class FichierFastX{
    private :

    //Attributs
    char * file_name;
    size_t * seq_pos;

    size_t nb_sequence;
    size_t format;
    SequenceFastX * sequenceFastx;//Pour gerer la memoire dynamique en evitant les fuites mémoire

    public :
    //Constructeur
    FichierFastX(const char * file_name = "NULL");
    FichierFastX(FichierFastX &f);

    //Destructeur
    ~FichierFastX();

    //Setter
    void setFileName(const char * file_name);
    void setNbSequence(size_t nb_sequence);

    //Getter
    const char * getFileName() const;
    size_t getNbSequence() const;
    size_t getFormat() const;
    SequenceFastX * getSequence(size_t numero_sequence);
    SequenceFastX * getSequenceFastX();
    size_t * getSeqPos();

    //Methode
    void toStream (ostream &os) const;
    friend ostream &operator << (ostream &os, const FichierFastX &f );
    FichierFastX & operator=(FichierFastX &f);
    int checkFormat();
    const size_t * indexation();

};


#endif