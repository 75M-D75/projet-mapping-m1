#include <string>
#include "SequenceFasta.h"
#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fstream>


SequenceFasta::SequenceFasta(string entete) {
    this -> entete = entete;
    this -> taille = 0;
}


SequenceFasta::SequenceFasta(SequenceFastX* &s) {
    this -> taille = s -> getTaille();
    this -> entete = s -> getEntete();
    
}

SequenceFasta::~SequenceFasta(){
    //cout << "Destructeur SequenceFasta" << endl;
    if (seq_enc)
    {
        /* code */
        delete seq_enc;
    }
}

// SequenceFasta::SequenceFasta(string entete, size_t taille, string seq) : SequenceFastX(entete, taille, seq){
//     //cout << "Fastaaa 4" << endl;
//     // this -> taille = taille;
//     // this -> entete = entete;
//     // //this -> seq    = seq;
//     // this -> seq_enc = new EncodedSequence(0);
//     // seq_enc -> encoderChaine(seq.c_str());
// }

SequenceFasta::SequenceFasta(string entete, size_t taille, EncodedSequence * seq_enc){
    this -> taille  = taille;
    this -> entete  = entete;
    this -> seq_enc = seq_enc;
}

SequenceFasta::SequenceFasta(string entete, size_t taille, string seq) : SequenceFastX(entete, taille, seq){
    //cout << "Fastaaa 4" << endl;
    // this -> taille = taille;
    // this -> entete = entete;
    // //this -> seq    = seq;
    // this -> seq_enc = new EncodedSequence(0);
    // seq_enc -> encoderChaine(seq.c_str());
}

// SequenceFasta::SequenceFasta(string entete, size_t taille, EncodedSequence * seq_enc) : SequenceFastX(entete, taille, seq_enc){
//     cout << "Fastaaa 2" << endl;
//     // this -> taille  = taille;
//     // this -> entete  = entete;
//     // this -> seq_enc = seq_enc;
// }

//Setter


//Getter
char * SequenceFasta::getSequence(){
    //cout << "Fastaaa G" << endl;
    /*cout << " -- " << endl;
    ifstream fichier(file_name);
    string sequence = "";
    int taille_entete;


    if ( fichier ) {  
        string ligne;
        fichier.seekg(position);
        getline(fichier, ligne);
        cout << "entete : " << ligne << endl;
        taille_entete = ligne.size();

        while( getline(fichier, ligne) && (int)(position + taille + taille_entete + 1) >= fichier.tellg() ){
            sequence = sequence + ligne;
        }  
        
        fichier.close();  
    }
    else{
        cerr << "Impossible d'ouvrir le fichier !  " << endl;
    }
    
    return sequence;*/
    //if(seq_enc)
        //seq = 
    /*if (seq.size()>0)
    {
        return seq.c_str();
    }*/
    return seq_enc -> decoderChaine();
}




//Methodes
char SequenceFasta::complement_cara(char carac){
    switch(carac){
        case 'A':
            return 'T';
        case 'T':
            return 'A';
        case 'G':
            return 'C';
        case 'C':
            return 'G';
        case 'Y':
            return 'R';
        case 'K':
            return 'M';
        case 'M':
            return 'K';
        case 'B':
            return 'V';
        case 'V':
            return 'B';
        case 'D':
            return 'H';
        case 'H':
            return 'D';
        case 'N':
            return 'N';
        case 'U':
            return 'A';
        default:
            return ' ';
    }
}

/*string SequenceFasta::sequence_complementaire(){
    string sequence_comp = this -> sequence;
    int n = sequence_comp.size();

    for(int i=0; i<n; i++){
        sequence_comp[i] = complement_cara(sequence_comp[i]);
    }    

    return sequence_comp;
}


string SequenceFasta::sequence_inverse(){
    string sequence_inverse = this -> sequence;
    int n = sequence_inverse.size();
    char temp;

    for(int i=0; i<n/2; i++){
        if ( sequence_inverse[n-i-1] != sequence_inverse[i] ) {//pour eviter des instructions atomique inuile

            temp = sequence_inverse[n-i-1];
            sequence_inverse[n-i-1] = sequence_inverse[i];
            sequence_inverse[i] = temp;
        }
    }    

    return sequence_inverse;
}*/
