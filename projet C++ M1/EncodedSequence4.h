#ifndef DEF_ENCODED_SEQUENCE_
#define DEF_ENCODED_SEQUENCE_

#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fstream>
#include <vector>
#include <regex>
#include <string.h>
#include <stdlib.h>


using namespace std;

class EncodedSequence{
	
private : 
	size_t taille;
	
	size_t n;
	size_t N;
	char * t; //tab avec octets

public :
	static size_t getByte(size_t i); //donne le numero de l'octet
	static size_t getPosInByte (size_t i);

	EncodedSequence (size_t n=0);
	EncodedSequence (const EncodedSequence &es);

	~EncodedSequence();

	//opérateur
	EncodedSequence & operator= (const EncodedSequence &es);

	//Méthode clear

	void clear();

	void reserve( size_t n);

	char operator[](size_t i) const ;
	//operateur d'écriture

	void setNucleotide (size_t i, char c);

	size_t getSize();

	void toStream( ostream &os) const ;

	EncodedSequence * encoderChaine(const char * chaine);

	char * decoderChaine();

	EncodedSequence & operator+=(char c);

	EncodedSequence RevComp() const;

};



#endif