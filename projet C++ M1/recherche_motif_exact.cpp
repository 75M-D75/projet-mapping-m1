#include <iostream>
#include <fstream> //pour lire les fichiers
#include <string>
#include <map> //pour les tableaux associatifs
#include <algorithm>    // std::min
#include <list>
#include <iterator>

using namespace std;
//Resultat attendu avec le text et le motif fourni 40 31 29

void affichageMPKMP_next(int * something_next, string P,int m){
    
    cout<<"P(Motif) :[";
    for(int i=1; i<m+1; i++){
        if( i<m )
            cout<<P[i]<<" ,";
        else
            cout<<P[i]<<"]"<<endl;
    }

    cout<<"\033[33m";//Couleur Jaune
    //Affichage mpkmp_next
    cout<<"Table    :[";
    for(int i=1; i<m+2; i++){
        if( i<m+1 )
            cout<<something_next[i]<<" ,";
        else
            cout<<something_next[i]<<"]"<<endl;
    }
    cout<<"\033[00m";//Couleur par defaut
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// NAÏF ///////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
void naif(string P, int m, string T, int n)
{
    int pos = 1;//Position de la fenetre
    int nb_compare = 0;//Nombre de comparaison

    while ( pos <= n - m + 1 ){

        //Traitement de la fenetre verifi si la fenetre = Motif
        int i = 1;//indice du caractere dans la fenetre
        while ( i <= m && P[i] == T.at(pos + i - 1) ){
            i++;
            nb_compare++;
        }

        //Compte la comparaison d'un carctere en cas d'inegalité
        if(i <= m)
            nb_compare++;

        //Affichage de la position du motif
        if ( i == m + 1 )
            cout<<P.c_str()<<" apparait a la position "<<pos<<endl;
        pos++;
    }

    cout<<"nombre de comparaison :"<<nb_compare<<endl;
}

//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// MP /////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
int * calcule_MP_next(string P, int m)
{
    //Initialisation
    int j = 0;
    int * mp_next = new int [m+2];
    mp_next[1] = 0;

    for (int i=1; i <= m; i++){
        while( j > 0 && P[i] != P[j] ){
            j = mp_next[j];
        }
        j++;
        mp_next[i+1] = j;
    }

    //Affichage MP_next
    affichageMPKMP_next(mp_next, P, m);

    return mp_next;
}

void MP(string P, int m, string T, int n)
{
    //Initialisation des variables
    int * mp_next = new int [m+2];
    mp_next = calcule_MP_next(P, m);

    int i = 1;
    int j = 1;
    int nb_compare = 0;//nombre de comparaison

    while ( j <= n ){

        //Verification de la correspondance avec le motif
        while(i > 0 && P[i] != T[j]){
            i = mp_next[i];
            nb_compare++;
        }

        //Compte la comparaison en cas d'egalité
        if(i > 0)
            nb_compare++;

        i++;
        j++;

        //Affichage de la position du motif dans le Texte
        if ( i == m + 1 ){
            cout<<P.c_str()<<" apparait a la position "<<j-i+1<<endl;
            i = mp_next[i];
        }
    }

    cout<<"nombre de comparaison :"<<nb_compare<<endl;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// KMP /////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
int * calcule_KMP_next(string P, int m)
{
    int * kmp_next = new int [m+2];
    kmp_next[1] = 0;
    int j = 0;
    
    //
    for ( int i=1; i<=m; i++ ){
        while( j>0 && P[i] != P[j] ){
            j = kmp_next[j];
        }
        j++;

        if( i==m || P.at(i+1) != P[j])
            kmp_next[i+1] = j;
        else
            kmp_next[i+1] = kmp_next[j];
    }

    //Affichage KMP_next
    affichageMPKMP_next(kmp_next, P, m);

    return kmp_next;
}

void KMP(string P, int m, string T, int n)
{
    int * kmp_next = new int [m+2];
    kmp_next = calcule_KMP_next(P, m);
    int i = 1;
    int j = 1;
    int nb_compare = 0;

    while( j <= n ){
        //Si il y'a pas de similitude
        while( i > 0 && P[i] != T[j] ){
            cout<<"Échec à la position "<<i<<" du motif et "<<j<<" du texte - P["<<i<<"]="<<P[i]<<" et T["<<j<<"]="<<T[j]<<endl;
            i = kmp_next[i];
            nb_compare++;
        }

        //Compte la comparaison en cas d'egalité
        if( i > 0 )
            nb_compare++;
        i++;
        j++;

        //Affichage de la position du motif dans le Texte
        if ( i == m+1 ){
            cout<<P.c_str()<<" apparait a la position "<<j-i+1<<endl;
            i = kmp_next[i];
        }
    }

    cout<<"nombre de comparaison :"<<nb_compare<<endl;
}

//////////////////////////////////////////////////////////////////////////////
////////////////////////////// Boyer-Moore ///////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
int * calcule_suff(string P, int m)
{
    int * suff = new int [m+1];
    suff[m] = m;
    int g = m;
    int f = m;

    for( int i=m-1; i>=1; i--){
        if ( i>g && suff[i+m-f] != i-g){
            suff[i] = min(suff[i+m-f], i-g);
        }
        else{
            f = i;
            g = min(g, i);
            while( g>0 && P[g] == P[g+m-f] ){
                 g = g-1;
            }
            suff[i] = f-g;
        }
    }

    cout<<"\033[33m";//Couleur Jaune
    cout<<"Table suf:[";
    for(int i=1; i<=m; i++){
        if( i<m )
            cout<<suff[i]<<" ,";
        else
            cout<<suff[i]<<"]"<<endl;
    }
    cout<<"\033[00m";//Couleur par defaut

    return suff;
}    

int * calcule_D(string P, int m)
{
     /* Initialisation */
    int * D = new int [m+20];
    //int * suff = new int [m+1];
    int * suff = calcule_suff(P, m);
    for(int i=1; i<=m; i++)
        D[i] = m;


    /*Cas 2*/
    int i = 1;
    for(int j=m-1; j>=0; j--){
        if(j==0 || suff[j] == j){
            while(i<=m-j){
                D[i] = m-j;
                i = i+1;
            }
        }
    }

    /*Cas 1*/
    for (int j = 1; j <=m-1; j++){
        D[m-suff[j]] = m-j;
    }


    cout<<"\033[33m";//Couleur Jaune
    //Affichage mpkmp_next
    cout<<"Table   D:[";
    for(int i=1; i<=m; i++){
        if( i<m )
            cout<<D[i]<<" ,";
        else
            cout<<D[i]<<"]"<<endl;
    }
    cout<<"\033[00m";//Couleur par defaut
    delete [] suff;

    return D;
}

map<char,int> calcule_R(string P, int m)
{
    map<char,int> R;


    //Initialisation    
    for(int i=0; i<256; i++){
        //R.insert( std::pair<char,int>(char(i), 0) );
        R[char(i)] = 0;
    }

    //
    for(int i=1; i<=m; i++){
        R[P[i]] = i;
    }
    

    cout<<"\033[33m";//Couleur Jaune
    cout<<"Table   R:[";
    for(int i=36; i<126; i++){
        if( i<125 )
            cout<<char(i)<<"\033[34m"<<"="<<"\033[33m"<<R[char(i)]<<" ,";
        else
            cout<<R[char(i)]<<"]"<<endl;
    }
    cout<<"\033[00m";//Couleur par defaut


    //Affichage que des caractere de P
    /*cout<<"\033[33m";//Couleur Jaune
    cout<<"Table   R:[";
    for(int i=1; i<=m; i++){
        if( i<m )
            cout<<P[i]<<"="<<R[P[i]]<<" ,";
        else
            cout<<P[i]<<"="<<R[P[i]]<<"]"<<endl;
    }
    cout<<"\033[00m";//Couleur par defaut*/

    return R;
}

void BM(string P, int m, string T, int n)
{
    int * D = calcule_D(P, m);
    map<char,int> R = calcule_R(P, m);
    
    int i;
    int pos = 1;
    int nb_compare = 0;

    while( pos <= n-m+1){
        i = m;
        while(i>0 && P[i] == T[pos+i-1]){
            
            nb_compare++;
            i--;
        }

        if(i>0)
            nb_compare++;

        if(i == 0){
            cout<<P.c_str()<<" apparait a la position "<<pos<<endl;
            pos = pos + D[1];
        }
        else{
            cout<<"Échec à la position "<<i<<" du motif et "<<(pos+i-1)<<" du texte - P["<<i<<"]="<<P[i]<<" et T["<<(pos+i-1)<<"]="<<T[pos+i-1]<<endl;
            cout<<"#Décalage mvs caractère="<<i-R[T[pos+i-1]]<<""<<" #Décalage bon suffixe="<<D[i]<<endl;
            pos = pos + max(D[i], i-R[T[pos+i-1]]);
        }
    } 
    delete [] D;
    cout<<"nombre de comparaison :"<<nb_compare<<endl;

}


map<char,std::list<int>> calcule_S(string P, int m)
{
    map<char, std::list<int>> S;

    //Remplissage de la table S du x de P le plus proche par la gauche de la position i(position d'echec)
    for(int i=m; i>=1; i--){
        S[P[i]].push_back(i);
    }

    //Affichage
    list <int> :: iterator it;

    cout<<"Table   S:[";
    for(int i=1; i<=m; i++){
        it = S[P[i]].begin();
            
        cout << P[i] << "=[";
        while(it != S[P[i]].end()){
            if( it ==  S[P[i]].begin())
                cout<<*it;
            else
                cout << ", " << *it;
            
            it++;
        }
        if( i != m)
            cout<<"], ";
        else
            cout<<"]]"<<endl;
        
    }
    
    return S;
}


void BM2(string P, int m, string T, int n)
{
    int * D = calcule_D(P, m);
    map<char, std::list<int>> S = calcule_S(P, m);
    
    int i;
    int pos = 1;
    int nb_compare = 0;

    while( pos <= n-m+1){
        i = m;
        while(i>0 && P[i] == T[pos+i-1]){

            nb_compare++;
            i--;
        }

        if(i>0)
            nb_compare++;

        if(i == 0){
            cout<<P.c_str()<<" apparait a la position "<<pos<<endl;
            pos = pos + D[1];
        }
        else{
            list <int> :: iterator it; 
            int s = 0;
            it = S[T[pos+i-1]].begin();
            
            while(it != S[T[pos+i-1]].end() && *it>=i ){
                it++;
            }


            if(*it != pos+i-1){
                s = *it;
            }
            //cout<<"Échec à la position "<<i<<" du motif et "<<(pos+i-1)<<" du texte - P["<<i<<"]="<<P[i]<<" et T["<<(pos+i-1)<<"]="<<T[pos+i-1]<<endl;
            //cout<<"#Décalage mvs caractère="<<i-s<<""<<" #Décalage bon suffixe="<<D[i]<<endl;
            pos += max(D[i], i-s);
        }
    } 
    delete [] D;

    cout<<"nombre de comparaison :" << nb_compare << endl;

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// main ///////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
/*int main(int argc, char** argv)
{
    if (argc != 3)
    {
        cout<<"Mauvaise utilisation du programme "<<argv[0]<<", on attend :"<<endl;
        cout<<argv[0]<<" ficMotif ficTexte"<<endl;
    }
    else
    {*/
        /* Lecture du motif */
        /*string nomFicMotif (argv[1]);
        ifstream IfFicMotif (nomFicMotif.c_str(), ios::in);
        if (!IfFicMotif)
        {
            cout<<endl<<"Impossible d'ouvrir le fichier : "<<nomFicMotif<<endl<<endl;
            return(0);
        }
        string P; char c;
        //On lit le fichier caractère par caractère
        while(IfFicMotif.get(c)){
            P += c;
        }
        //On élimine l'éventuel retour à la ligne en fin de fichier
        if (P.size() >1 && (int)P[P.size()-1]==10)
            P=P.substr(0,P.size()-1);

        P='#'+P; //Pour que la première lettre de P soit à l'indice 1
        int m=P.size()-1;*/

        /* Lecture du texte */
        /*string nomFicTexte(argv[2]);
        ifstream IfFicTexte (nomFicTexte.c_str(), ios::in);
        if (!IfFicTexte)
        {
            cout<<endl<<"Impossible d'ouvrir le fichier : "<<nomFicTexte<<endl<<endl;
            return(0);
        }
        string T;
        //On lit le fichier caractère par caractère
        while(IfFicTexte.get(c)){
            T += c;
        }
        //On élimine l'éventuel retour à la ligne en fin de fichier
        if (T.size() >1 && (int)T[T.size()-1]==10)
            T=T.substr(0,T.size()-1);

        T='#'+T; //Pour que la première lettre de T soit à l'indice 1
        int n=T.size()-1;
        
        cout<<"********************************************************************"<<endl;
        cout<<"Recherche du motif de taille "<<m<<" P="<<P.substr(1,P.size())<<endl;
        //On n'affiche si il n'est pas trop long
        cout<<"Dans un texte de taille "<<n<<endl;
        if(T.length() < 50){
            cout<<T.c_str()<<endl;
        }
        //<<" T="<<T.substr(1,T.size())<<endl;
        cout<<"********************************************************************"<<endl<<endl<<endl;

        cout<<"************* Recherche naïve *************"<<endl;
        naif(P,m,T,n);
        cout<<"###############################"<<endl<<endl;

        cout<<"************* Recherche avec MP *************"<<endl;
        MP(P,m,T,n);
        cout<<"################################"<<endl<<endl;

        cout<<"************* Recherche avec KMP *************"<<endl;
        KMP(P,m,T,n);
        cout<<"################################"<<endl<<endl;

        cout<<"************* Recherche avec Boyer-Moore *************"<<endl;
        BM(P,m,T,n);
        cout<<"################################"<<endl;

        cout<<"************* Recherche avec Boyer-Moore 2 *************"<<endl;
        BM2(P,m,T,n);
        cout<<"################################"<<endl;
    }
}*/
