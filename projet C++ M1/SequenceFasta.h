#ifndef DEF_SEQUENCE_FASTA
#define DEF_SEQUENCE_FASTA
#include <string>
#include "SequenceFastX.h"


using namespace std;

class SequenceFasta : public SequenceFastX {
    private :
    //Attributs


    public :
    SequenceFasta(string entete="");
    SequenceFasta(string entete, size_t taille, string seq);
    SequenceFasta(string entete, size_t taille, EncodedSequence * seq_enc);
    SequenceFasta(SequenceFastX* &s);
    virtual ~SequenceFasta();

    //Setter


    //Getter
    char * getSequence();
    
    //Methode
    char complement_cara(char carac);
    string sequence_complementaire();
    string sequence_inverse();

};

#endif