#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> /* close */
#include <netdb.h>  /* gethostbyname */
#include <sys/ipc.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <string.h>
#include <fstream>
#include <string>
#include <vector>
#include <regex>

#include "recherche_motif_exact.h"
#include "FichierFastX.h"
#include "SequenceFastq.h"
#include "ReadFastX.h"
#include "struct_dico.h"
 
using namespace std;


string color(string s){
    return "\033[33m" + s + "\033[00m";
}

void chargement(float pourcentage){
    int nb_c = 25;
    //system("clear");
    cout << "[";
    for (int i = 0; i < nb_c; ++i){
        if( (float)((float)i*(100/nb_c)) < (float) (pourcentage) ){
            cout << "#";
        }
        else{
            cout << "_";
        }
    }
    cout << "] : " << (int)pourcentage << "%" << endl;
}

ofstream & head(ofstream & os){
    os << "<!DOCTYPE html><html lang='en'><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'><meta name='description' content=''><meta name='author' content=''><title>Mapping Table</title><link href='table.css' rel='stylesheet'><link href='style_cursor.css' rel='stylesheet'><script src='https://code.jquery.com/jquery-1.10.2.js'></script><script src='https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js'></script>";
    os << "<script src='https://code.jquery.com/jquery-1.10.2.js'></script><script src='https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js'></script><link href=\"https://fonts.googleapis.com/css?family=Poppins\" rel=\"stylesheet\"><script src=\"https://cdn.jsdelivr.net/npm/baffle@0.3.6/dist/baffle.min.js\"></script>";
    os << "</head><body><h1 class='data'><span class='blue'>&lt;</span>Table<span class='blue'>&gt;</span> <span class='yellow'>Mapping</pan></h1>";
    os << "<h2 class='data'>Created with love by <a href='#' target='&nbsp;blank' class='cote'>M-D</a></h2><div class=\"cursor\"></div>";
    os << "<table class='container'><div><h1 class=\"phrase_equipe\"> </h1></div><thead><tr><th><h1>Infos</h1></th><th><h1>Nucleotides</h1></th></tr></thead><tbody class=\"float\">" << endl;
    return os;
}


ofstream & add_tr(ofstream & os, string infos, string sequence){
    os << "<tr>";
    os <<  "<td>" << infos    << "</td>";
    os <<  "<td>" << sequence << "</td>";
    os << "</tr>" << endl;
    return os;
}

ofstream & foot(ofstream & os){
    // os << "<table class='container'><thead><tr><th><h1>Infos</h1></th><th><h1>Nucleotides</h1></th></tr></thead<tbody><tr><td>Read</td><td>CGTTGTGCACATGTACCCTAGAACTTAAAGTATAATTTTTAAAAACNANGNAATTTATTTGCGNATCTTNATATCTTTAGCTCTGCCACCNAANANNTGCTATCTCTG</td></tr><tr><td>Sequence</td><td>TGCCAGTTCATTACTTCCTAAAACCAGCAGTTTATTGCCCAACACAGACAGAATGAATGTAGAAAAGGCTGAACTCTGCCATAAAAACAAACAGCCTGCCTTAGGAAGGAACCAAGAGAGCAGTCTCGATGAAAGTAAGGAAATATGTAGCCCTGGGAAGACCCCAGGTGCCTGTGAGCGGCATGAGCTGAATGACCATCATCTGTGTGAGAGGCAAGGCCTAGAGGAGAAGCCGCAGCACCCTGAGAGCCCTAGAGGTAATCCTCAGAACTGCCTGTCTGGAACCAAACTGAAAAGCAGCATTCAGAAAGTTAATGAGTGGTTATCCAGAAGTAGTGACATTTTAGCCTCTGATAACTCCAACGGTAGGAGCCATGAGCAGAGCGCAGAGGTGCCTAGTGCCTTAGAAGATGGGCATCCAGATACCGCAGAGGGAAATTCTAGCGTTTCTGAGAAGACTGACCCAGTGGCTGACAGCACTGATGATGCCTGGCTACATGTGCCTAAAAGAAGCTGTCTCAGGCCTGCAGAAAACAATATTGAAGATAAAATATTTGGAAAAACCTATCGGAGAAAGTCAGGTCACCCTAATTTGAATTATGTAACTGAAAACTTGTTTGCTGAAGCTGTGGCTCCTGATTCCTTGATCCCTCTAGAGGCTCCCAAAAACACCAGGTTAAAGCGTAAAGGAAGGAGCATAGCTGACCTGCAGCCTGAGGATTTCATCAGGAAGACGGACGTTCCAGTTATTCACAAAACCCCTGAAAAGAAAAATCACTCTGTTGACCAAATTCTCAAAAGAGAACAAAGTGACCAAGTGATGAACACGGCTAACAGTCTTCCTGAGCAGAAAGCCCTAGGTGGTCATGTAGGGGAAGTGAAAGATGTTCAGGTATTAGAGCTGTTCTCTGCGGAGAAAGAATCCACTTTCAGAACTGGAACAGAGCCTGTAGCTGGCAGCACAAACCATGGGGAGCTTGAATTAAATAGTAGAAATGCCAAAATGACAAGAAAAGACAGGCTGAGGAAGAAGCCTTCAGCCAGGATCGTCCGTGCTCTTGAGCTCGTAGTTGATAGAAACCCAAGCTCTTCTAATGAGAGTGAACTGCAGATCGATAGCTATCCCAGCAGTGAAGAGATAAGGAAAGGAACTAATTCTGAACAGAAGCAAATCAGACGCAGCAGAAGGCTTCGGCTGCTATCAGAAGAAATTGTGGTTGGAGCCAAGAAGGCCCATAAGCCAGATGACCAAGCAGAAGAAAGTTGTATCAGTGAAGTTTTCCCAGAACTAAAAATAGGAAACGTGCCTGCCTGTGCTACTGACAGTCTAACTACTGATAGGGATCAAGTGTTAGCTAGCTGCAGTTTCACAGAAGAAAGAGATGAAAGGAGCCTGGAAGCAATCCCAAGCAGCAAAGACCAAGATCTGCCCTTGAATGGAGGGGAGGGGTTGCAAGGTGAAAGAGCCCCAGGAAGCCTGGAGGCTTTGGAGGTTCCTGATACTGATTGGGACACTCAGGACAGTACCTCATTGTTTCCAGCTGATACTCCCCAAAATTCAAAAGCAGGACCCAGTCCTCACAGAAGCCACAGTGAAATAATGGAAACCCCCAAGGAACTCTTAGATGGTTGTTCATCCAAAAACACTGAAAGTGACGAAGAAGATTTGAGGAGCCTGATGAGACAGGAAGTTAAAAATGCCTCCAAGACAACCACAGAAATGGAGGATAGTGAACTCGACACCCAGTATTTACAAAATACCTTCAAACGTTCAAAGCGCCAGTCATTTGCTCTGCGTTCTAGCCCAAGGCAGGAATGTAGGAAACCCTCTGCTGTCCCTGGGACTGTAAATCAGCAGAGTCCAGATAACACCACAGATTGTGGGGGCCAGGAAAAAGAAAAGCAGGGAAACAGAGAATCAAACAAGCCTGTGTGGCCAAAGTCTGCAGTCATGAGCTTAGCTGCGGCTTGTCAGACAGAGGAGAGGAAGCCAGGTGTTTATGCCAAATGTAGCACCACAGAAGTGTCCAGGCTTTGTCACATAGCTCCATTACATGGTGTCATTGACTGTGAACACATTGCTGGAAACAACCAGGGAATTTCGCAAGTTCCTGATCAAAAACCATCAGTTTCTCCAGCAAGGTCATCTGCTAGTAAAACTATAAATACAAAAAACCTCCTGGAGGAAAGGCTTGATGAACAGACCACATGTCCTGAAACAGCTATGGGAAATGAAAGCTTAGCCCAAAGCAGCTTAAGTCCAGTAAGCCAAAGTAATAGCAGAGAATATATTTCTAAAGCAACTGACTTAAATAGATTTATCAGCATAGGCTCTAGTGGAGAAGGCAGTCAGGCACAAAAAGGTGAAAACAAAGAATCTGAATTAAATACACAACCCAAATTAAAGCTTGTGCAACCCCAAATCTGTCAACAAAGCTTTCCTCAGGATAATTGCAAAGAGTCTAAAAGAAAAGGGAAGGGAGGAAATGGAAAATTAGCTCAGGCCATCAGTACAGATTCATCTCCATGTTTAGAACAAACTAAAGAGAGTACACATTCTTCTCAGGTTTGTTCTGAGACACCTGATGACCT</td></tr>";
    // os << "<tr><td>0</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTTGTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GTT&nbsp;GTT&nbsp;&nbsp;</td></<tr><td>1</td><td>3654</t</tr><tr><td>2</td><td>2002</td></tr><tr><td>3</td><td>4623</td></tr></tbody>";
    // os << "</table>";
    // os << "</body></html>" << endl;
    os << "</tbody></table><script src='table.js'></script></body></html>" << endl;
    return os;
}

//Test unitaire
int main(int argc, char ** argv){

    char * read_file;

    if ( argc != 3 ) {
        cout << "Usage : " << argv[0] << " nom_fichier " << "read_file" << endl;
        return -1;
    }


    char * nom_fichier = argv[1];
    read_file = argv[2];

    FichierFastX f(nom_fichier);

    cout << f.getFileName() << endl;
    cout << "Acces au fichier sequence " << color("[OK]") << endl;
    cout << "Nombre sequence " << f.getNbSequence() << endl;

    FichierFastX f_read;
    f_read.setFileName(read_file);
    cout << "Acces au fichier sequence " << color("[OK]") << endl;
    cout << "Nombre sequence " << f_read.getNbSequence() << endl;
    const size_t k = 3;

    ofstream file_algnement_visual;
    ofstream file_position_kmer;
    ofstream file_algnement_visual_rev_comp;
    ofstream file_position_kmer_rev_comp;

    ofstream file_algnement_visual_html;

    size_t last;

    for(size_t i=1; i<=f.getNbSequence(); i++){
        char chemin[200];
        sprintf(chemin, "./alignement/alignement_%d.json", (int)i);
        file_algnement_visual.open(chemin);

        sprintf(chemin, "./alignement/alignement_rev_comp%d.json", (int)i);
        file_algnement_visual_rev_comp.open(chemin);

        sprintf(chemin, "./positions/position_%d.as", (int)i);
        file_position_kmer.open(chemin);

        sprintf(chemin, "./positions/position_rev_comp%d.as", (int)i);
        file_position_kmer_rev_comp.open(chemin);

        sprintf(chemin, "./html/alignement_%d.html", (int)i);
        file_algnement_visual_html.open(chemin);

        //char * chaine = f.getSequence(i) -> getSequence();
        //string T(chaine);
        f.getSequence(i);
        EncodedSequence * chaine = f.getSequenceFastX() -> getSequenceEncoder();
        EncodedSequence * T = new EncodedSequence(*chaine);
        //cout << "Sequence : " << i << "\n" << chaine << endl;
        for (size_t e = 1; e <= f_read.getNbSequence(); ++e){
            //???
            system("clear");
            cout << "Sequence : " << i << endl;
            chargement((float)((float)e/f_read.getNbSequence())*100);
            chargement((float)((float)i/f.getNbSequence())*100);
            vector<vector<size_t>> tab_position_k;
            
            //string read_k;
            vector<string> bloc_k_mer;

            //char * chaine_read = f_read.getSequence(e) -> getSequence();
            f_read.getSequence(e);
            EncodedSequence * chaine_read = f_read.getSequenceFastX() -> getSequenceEncoder();

            EncodedSequence read_k(*chaine_read);
            ///string read_k_b(chaine_read);
            //read_k = read_k_b;
            // cout << "A partir du fichier de seuqence " << f.getFileName() << endl;
            // cout << "Et a partir du fichier de read " << f_read.getFileName() << endl;
            // cout << "Recherche des correspondances du read : " << e << "\n" << f_read.getSequenceFastX() -> getEntete() << "\n" << read_k << endl;
            // cout << "\nSur la Sequence : " << i << "\n" << f.getSequenceFastX() -> getEntete() << "\n" << chaine << endl;

            // file_position_kmer << "A partir du fichier de seuqence " << f.getFileName() << endl;
            // file_position_kmer << "Et a partir du fichier de read " << f_read.getFileName() << endl;
            // file_position_kmer << "Recherche des correspondances du read : " << e << "\n" << f_read.getSequenceFastX() -> getEntete() << "\n" << read_k << endl;
            // file_position_kmer << "\nSur la Sequence : " << i << "\n" << f.getSequenceFastX() -> getEntete() << "\n" << chaine << endl;

            file_position_kmer << "A partir du fichier de seuqence " << f.getFileName() << endl;
            file_position_kmer << "Et a partir du fichier de read " << f_read.getFileName() << endl;
            file_position_kmer << "Recherche des correspondances du read : " << e << "\n" << f_read.getSequenceFastX() -> getEntete() << "\n";
            read_k.toStream(file_position_kmer);
            file_position_kmer << "\nSur la Sequence : " << i << "\n" << f.getSequenceFastX() -> getEntete() << "\n"; 
            T -> toStream(file_position_kmer);

            //bloc_k_mer = naive_tab_k_mer_s("#" + T, read_k, k, tab_position_k);
            //bloc_k_mer = naive_tab_k_mer_s_last("#" + T, read_k, k, tab_position_k, last);

            bloc_k_mer = naive_tab_k_mer_s_last_enc(*T, read_k, k, tab_position_k, last);
            
            //delete chaine_read;
            //chaine_read = NULL;
            // string aligne((last<read_k.size() ? read_k.size() : last), '_');
            // //string aligne_num((last<read_k.size() ? read_k.size() : last), '_');
            // cout << "Position des " << bloc_k_mer.size() << " k_mer de taille "<< k << " dans la sequence " << i << endl; 
            
            // for (size_t i = 0; i < bloc_k_mer.size(); ++i){
            //     //cout <<"positions k_mer " << i << " : " << bloc_k_mer[i] << " = ";
            //     printf("positions k_mer %4d : %s = ", (int)i, bloc_k_mer[i].c_str());
            //     //cout << bloc_k_mer[i] << " i " << i;
            //     for (size_t e = 0; e < tab_position_k[i].size(); ++e){
            //         //cout << " " << tab_position_k[i][e] << ", ";
            //         printf(" %5d, ", (int)tab_position_k[i][e]);
            //         if(i<3)
            //             for (size_t a = 0; a < k; ++a)
            //             {
            //                 aligne[tab_position_k[i][e]+a-1] = bloc_k_mer[i][a];
            //                 //aligne_num[tab_position_k[i][e]+a-1] = (int)i;
            //                 //if(i<9)
            //                   //  sprintf(&aligne_num[tab_position_k[i][e]+a-1], "%d", i);
            //             }
            //     }
            //     cout << "\n";
            // }
            // cout << "\n" << endl;


            string aligne("");

            file_algnement_visual << "Aligne : \n";
            file_algnement_visual << "READ:";
            read_k.toStream(file_algnement_visual);
            file_algnement_visual << "SEQ :";
            chaine -> toStream(file_algnement_visual);
            //file_algnement_visual << aligne << endl;
            if(e==1){
                head(file_algnement_visual_html);
                file_algnement_visual_html << "<tr> ";
                file_algnement_visual_html <<  "<td>" << "READ:" << e    << "</td>";
                file_algnement_visual_html <<  "<td>";
                read_k.toStream(file_algnement_visual_html);
                file_algnement_visual_html << "</td>";
                file_algnement_visual_html << "</tr>" << endl;

                file_algnement_visual_html << "<tr>";
                file_algnement_visual_html <<  "<td>" << "SEQU:" << i << "</td>";
                file_algnement_visual_html <<  "<td>";
                T -> toStream(file_algnement_visual_html);
                file_algnement_visual_html << "</td>";
                file_algnement_visual_html << "</tr>";
                file_algnement_visual_html << "</tbody>" << endl;

                file_algnement_visual_html << "<tbody class=\"no_float\">";
                file_algnement_visual_html << "<tr> ";
                file_algnement_visual_html <<  "<td>" << "READ:" << e    << "</td>";
                file_algnement_visual_html <<  "<td>";
                read_k.toStream(file_algnement_visual_html);
                file_algnement_visual_html << "</td>";
                file_algnement_visual_html << "</tr>" << endl;


                file_algnement_visual_html << "<tr>";
                file_algnement_visual_html <<  "<td>" << "SEQU:" << i << "</td>";
                file_algnement_visual_html <<  "<td>";
                T -> toStream(file_algnement_visual_html);
                file_algnement_visual_html << "</td>";
                file_algnement_visual_html << "</tr>" << endl;
            }
    
            //cout << "bloc_k_mer "  << bloc_k_mer.size() << endl; 
            for (size_t i = 0; i < bloc_k_mer.size(); ++i){
                char chif[12];
                char chif_2[12];
                sprintf(chif, "%5d", (int)i);
                //printf("positions k_mer %4d : %s = ", (int)i, bloc_k_mer[i].c_str());
                //fprintf(file_positions, "positions k_mer %4d : %s = ", (int)i, bloc_k_mer[i].c_str());
                file_position_kmer << "positions k_mer " << chif << " : " << bloc_k_mer[i].c_str() << " = ";

                if (tab_position_k[i].size() > 0){
                    //aligne += chif;
                    file_algnement_visual << chif;
                }

                //cout << bloc_k_mer[i] << " i " << i;
                for (size_t e = 0; e < tab_position_k[i].size(); ++e)
                {
                    //printf(" %5d, ", (int)tab_position_k[i][e]);
                    sprintf(chif_2, "%5d", (int)tab_position_k[i][e]);
                    file_position_kmer << " " << chif_2 << ", ";
                    //fprintf(file_positions, " %5d, ", (int)tab_position_k[i][e]);
                    if(e == 0){
                        for (size_t a = 0; a < tab_position_k[i][e]-1; ++a)
                        {
                            //aligne[tab_position_k[i][e]+a-1] = bloc_k_mer[i][a];
                            //aligne += " ";
                            file_algnement_visual << " ";
                            aligne += "&nbsp;";
                        }
                        aligne += bloc_k_mer[i];
                        file_algnement_visual << bloc_k_mer[i];
                    }
                    else{
                        for (size_t a = tab_position_k[i][e-1]+(k-1); a < tab_position_k[i][e]-1; ++a)
                        {
                            //aligne[tab_position_k[i][e]+a-1] = bloc_k_mer[i][a];
                            //aligne += " ";
                            file_algnement_visual << " ";
                            aligne += "&nbsp;";
                            //&nbsp;
                        }
                        if((tab_position_k[i][e-1]+(k-1)) < tab_position_k[i][e]-1 ){
                            aligne += bloc_k_mer[i];
                            file_algnement_visual << bloc_k_mer[i];
                        }
                        else
                            for (size_t x = k-((tab_position_k[i][e]+(k-1))-(tab_position_k[i][e-1]+(k-1))); x < k; ++x){
                                aligne += bloc_k_mer[i][x];
                                file_algnement_visual << bloc_k_mer[i][x];
                            }
                    }
                }

                if (tab_position_k[i].size() > 0){
                    aligne += "\n";
                    file_algnement_visual << "\n";
                    if (e==1 ){
                        add_tr(file_algnement_visual_html, chif, aligne);
                    }
                }
                
                aligne = "";
                file_position_kmer << "\n";
            }
            file_position_kmer << "\n" << endl;
            if(e==1){
                foot(file_algnement_visual_html);
            }

            

            // file_algnement_visual << "Aligne : \n";
            // file_algnement_visual << "READ:";
            // read_k.toStream(file_algnement_visual);
            // file_algnement_visual << "SEQ :";
            // chaine -> toStream(file_algnement_visual);
            // file_algnement_visual << aligne << endl;

            //Atte
            tab_position_k.clear();
            
            //string read_k;
            bloc_k_mer.clear();



            bloc_k_mer = naive_tab_k_mer_s_last_enc_rev_comp(*T, read_k, k, tab_position_k, last);

            file_position_kmer_rev_comp << "A partir du fichier de seuqence " << f.getFileName() << endl;
            file_position_kmer_rev_comp << "Et a partir du fichier de read " << f_read.getFileName() << endl;
            file_position_kmer_rev_comp << "Recherche des correspondances du read : " << e << "\n" << f_read.getSequenceFastX() -> getEntete() << "\n";
            read_k.toStream(file_position_kmer_rev_comp);
            file_position_kmer_rev_comp << "\nSur la Sequence : " << i << "\n" << f.getSequenceFastX() -> getEntete() << "\n"; 
            T -> toStream(file_position_kmer_rev_comp);

            aligne = "";

            file_algnement_visual_rev_comp << "Aligne : \n";
            file_algnement_visual_rev_comp << "READ:";
            read_k.toStream(file_algnement_visual_rev_comp);
            file_algnement_visual_rev_comp << "SEQ :";
            chaine -> toStream(file_algnement_visual_rev_comp);
            //file_algnement_visual_rev_comp << aligne << endl;

            //
            for (size_t i = 0; i < bloc_k_mer.size(); ++i){
                char chif[12];
                char chif_2[12];
                sprintf(chif, "%5d", (int)i);
                //printf("positions k_mer %4d : %s = ", (int)i, bloc_k_mer[i].c_str());
                //fprintf(file_positions, "positions k_mer %4d : %s = ", (int)i, bloc_k_mer[i].c_str());
                file_position_kmer_rev_comp << "positions k_mer " << chif << " : " << bloc_k_mer[i].c_str() << " = ";

                if (tab_position_k[i].size() > 0){
                    //aligne += chif;
                    file_algnement_visual_rev_comp << chif;
                }
                //cout << bloc_k_mer[i] << " i " << i;
                for (size_t e = 0; e < tab_position_k[i].size(); ++e)
                {
                    //printf(" %5d, ", (int)tab_position_k[i][e]);
                    sprintf(chif_2, "%5d", (int)tab_position_k[i][e]);
                    file_position_kmer_rev_comp << " " << chif_2 << ", ";
                    //fprintf(file_positions, " %5d, ", (int)tab_position_k[i][e]);
                    if(e == 0){
                        for (size_t a = 0; a < tab_position_k[i][e]-1; ++a)
                        {
                            //aligne[tab_position_k[i][e]+a-1] = bloc_k_mer[i][a];
                            //aligne += " ";
                            file_algnement_visual_rev_comp << " ";
                        }
                        //aligne += bloc_k_mer[i];
                        file_algnement_visual_rev_comp << bloc_k_mer[i];
                    }
                    else{
                        for (size_t a = tab_position_k[i][e-1]+(k-1); a < tab_position_k[i][e]-1; ++a)
                        {
                            //aligne[tab_position_k[i][e]+a-1] = bloc_k_mer[i][a];
                            //aligne += " ";
                            file_algnement_visual_rev_comp << " ";
                        }
                        if((tab_position_k[i][e-1]+(k-1)) < tab_position_k[i][e]-1 ){
                            //aligne += bloc_k_mer[i];
                            file_algnement_visual_rev_comp << bloc_k_mer[i];
                        }
                        else
                            for (size_t x = k-((tab_position_k[i][e]+(k-1))-(tab_position_k[i][e-1]+(k-1))); x < k; ++x){
                                //aligne += bloc_k_mer[i][x];
                                file_algnement_visual_rev_comp << bloc_k_mer[i][x];
                            }
                    }
                }

                if (tab_position_k[i].size() > 0){
                    //aligne += "\n";
                    file_algnement_visual_rev_comp << "\n";
                }
                
                
                file_position_kmer_rev_comp << "\n";
            }
            file_position_kmer_rev_comp << "\n" << endl;


            // file_algnement_visual << "Aligne : \n";
            // file_algnement_visual << read_k << endl;
            // file_algnement_visual << chaine << endl;
            // file_algnement_visual << aligne << endl;

            // file_algnement_visual << "Aligne : \n";
            // file_algnement_visual << "READ:" << read_k << endl;
            // file_algnement_visual << "SEQ :" << chaine << endl;
            // file_algnement_visual << aligne << endl;

            

            // file_algnement_visual_rev_comp << "Aligne : \n";
            // file_algnement_visual_rev_comp << "READ:";
            // read_k.toStream(file_algnement_visual_rev_comp);
            // file_algnement_visual_rev_comp << "SEQ :";
            // chaine -> toStream(file_algnement_visual_rev_comp);
            // file_algnement_visual_rev_comp << aligne << endl;
            //file_algnement_visual << aligne_num << endl;
            //if( (int)((float)((float)i/f.getNbSequence())*100) % 2 == 0 ){
                //???

                chargement((float)((float)i/f.getNbSequence())*100);
            //}
            

        }
        //delete [] chaine;
        //delete chaine;
        //chaine = NULL;
        delete T;
        T = NULL;
        file_algnement_visual.close();
        file_algnement_visual_rev_comp.close();
        file_position_kmer.close();
        file_position_kmer_rev_comp.close();
        file_algnement_visual_html.close();
    }


    return 0;
}

