#include <iostream>
#include <libgen.h>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

struct Cmp{
    const string &ref;
    size_t n;

    Cmp(const string &r):ref(r),n(r.size()){}

    bool operator()(size_t i, size_t j) const {
        int v = 0;
        while(!v && (i<n) && (j<n)){//v=0
            
            if(ref[i] < ref[j]){
                v=-1;
            } 
            else {
                if(ref[i] > ref[j]){
                    v=1;
                }
                else{
                    ++i;
                    ++j;
                }
            }
        }
        
        if(i==n){
            v=-1;
        }
        else{
            if(j==n) {
                v=1;
            }
        }
        
        return(v<0);
    }
};


// void tri_bulles(vector<size_t>& tab, const string &s)
// {
//     Cmp cmp(s);

//     bool tab_en_ordre = false;
//     //int taille = tab.size();
//     int taille = s.size();
//     while(!tab_en_ordre){
//         tab_en_ordre = true;
//         for(int i=0; i < taille-1; i++){
//             if(tab[i] > tab[i+1]){
//                 swap(tab[i], tab[i+1]);
//                 tab_en_ordre = false;
//             }
//         }

//         for(int i=0; i < taille-1; i++){
//             if( !cmp(s.substr(tab[i]), s.substr(tab[i+1])) ){
//                 swap(tab[i], tab[i+1]);
//                 tab_en_ordre = false;
//             }
//         }
//         taille--;
//     }
// }


void SA_BWT(const string &chaine, vector<size_t> &SA, vector<size_t> &BWT){
    if( SA.size() != chaine.size() ){
        SA.resize(chaine.size());
    }

    if( BWT.size() != chaine.size()){
        BWT.resize(chaine.size());
    }

    for(size_t i=0; i<SA.size(); ++i){
        SA[i] = i;
    }

    sort(SA.begin(), SA.end(), Cmp(chaine));

    for(size_t i=0;i<SA.size();++i){
        BWT[i] = (SA[i]?SA[i]-1:chaine.size()-1);
    }
}


int main(int argc, char ** argv){
    if( argc!=2){ //argc = taille du tableau argv
        cerr<<"usage: "<<basename(argv[0]);
        
        return 1;
    }
    string s(argv[1]);

    cout<<"chaine a verifier : "<<argv[1]<<" "<< endl;
    vector<size_t> SA(s.length());
    vector<size_t> BWT;
    //iota(SA.begin(), SA.end(),0);
    // for(size_t i=0;i<SA.size();++i){
    //     SA[i] = i;
    // }

    SA_BWT(s, SA, BWT);

    cout<<"SA=[";
    for(size_t i=0;i<s.size();++i){
        cout<<(i?", ":"")<<SA[i];}
    
    cout<< "]"<<endl;


    //sort(SA.begin(),SA.end(), Cmp(s)); //comparer les suffixes par ordre lexicographique :
    for(size_t i=0;i<s.length();++i){
        cout << "-SA[" << i << ":" << SA[i] << "]=>" << s.substr(SA[i]) /*<< s.substr(0, (SA[i]?SA[i]:0))*/ << endl;
    }

    for(size_t i=0;i<s.length();++i){
        cout << "-BWT[" << i << ":" << SA[i] << "]=>" << s.substr(BWT[i]) << endl;
    }
    

    cout<<"SA=[";
    for(size_t i=0; i<s.size(); ++i){
        cout<<(i?", ":"")<<SA[i];}
    
    cout<< "]"<<endl;
}
