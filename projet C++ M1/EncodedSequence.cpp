#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "EncodedSequence.h"

//Constructeurs
EncodedSequence :: EncodedSequence(size_t n) :
	n(0), 
	N(n? getByte(n)+1:0), 
	t(N ? new char [N] :NULL) 
{
	//cout << "Constructeurs EncodedSequence " << endl;
	//cout << N << " N "<< n << " " << endl;
}

EncodedSequence :: EncodedSequence (const EncodedSequence &es):
	n(es.n),
	N(es.N),
	t(N ? new char[N]:NULL) 
{
	//cout << " BBB " << endl;
	//cout << " es " << endl;
	//cout << &es << endl;
 	for (size_t i=0; i<N; ++i)
	{
		t[i]=es.t[i];
		copy(es.t, es.t+N,t);
	}
}


EncodedSequence :: ~EncodedSequence()
{
	//cout << "Destructor EncodedSequence" << endl;
	clear(); //=méthode
}

//Methide
EncodedSequence &EncodedSequence :: operator= (const EncodedSequence &es)
{
	//cout << " BBB " << endl;
	if(this != &es)
	{
		clear();
		n=es.n;
		N = es.N;
		t= n? new char [N] :NULL;
		copy(es.t, es.t +N,t);
	}
	return *this;
}


void EncodedSequence :: clear(){
	if(N){
		delete []t;
		t=NULL;
		N=0;
		n=0; //n est la taille
	}
}

void EncodedSequence :: reserve( size_t n){
	size_t N = getByte(n)+1;
	if(N > this -> N){
		char *t = new char [N];
		copy(this -> t, this -> t + this -> N, t);
		delete [] this -> t;
		this -> t = t;         // pour coder 9 nuc sur trois octets 
		this -> N = N;
	}
}


size_t EncodedSequence :: getByte(size_t i)
{
	return i? (i-1)>>2 : 0;
}



size_t EncodedSequence :: getPosInByte (size_t i){
	return i? (i-1)&3 :0;
}

//Local methode
char encode(char c){
	return (((c=='a') || (c=='A'))
	?0
	:(((c=='c')||(c=='C'))
		?1
		:(((c=='g')||(c=='G'))                 
			?2
			:(((c=='t')|| (c=='T')||(c=='u')|| (c=='U'))
				?3
	:(mrand48()&3))))); //génère des nombres aléatoires, entre 0 et 3 inclu
}


char decode (char c){
	return ((c==0) ?'A'
	:((c==3)
		?'T'
		:((c==2) ?'G':'C')));
}


// Operateur d'acces en lecture
char EncodedSequence :: operator[](size_t i) const 
{
	try{
		if (i>n){
			throw "out of bound";
		}
	}
	catch(const char * chaine){
		cerr << chaine << endl;
		throw " ";
	}
	
	char c = t[getByte(i)];
	c >>= ((3-getPosInByte(i))<<1);
	return decode(c&3);
}

//

//operateur d'écriture
void EncodedSequence :: setNucleotide(size_t i, char b){
	reserve(i); 
	if (i > n){
		n = i;
	}

	char &c = t[getByte(i)];

	size_t shift = ((3 - getPosInByte(i))<<1);
	c &= ~(3<<shift);
	c |= (encode(b) << shift);
}

size_t EncodedSequence :: getSize(){
	return n;
}

void EncodedSequence :: toStream( ostream &os ) const {
	for (size_t i=1; i<=n; ++i)
	{
		os << operator[](i);
	}
	os << endl;
}


EncodedSequence &EncodedSequence :: operator+=(char c){
	//cout << " BBB " << endl;
	reserve(n+1);
	++n;
	char &b = t[getByte(n)]; 
	size_t shift = ((3 - getPosInByte(n))<<1);
	b &= ~(3<<shift);
	b |= (encode(c) << shift);
	return *this;

}

unsigned char swich(unsigned char c){

	unsigned char d = 0;
	int i;
	for(i=0; i<4; i++){
		//if(c&(3<<i*2)){
			unsigned char cd = (c&(3<<i*2))>>(i*2);
			d = d|((cd<<((3-i)*2)));
		//}
	}
    return d;
}

unsigned char swich_acess(unsigned char c, size_t n){

	unsigned char d = 0;
	int i;
	for(i=0; i<4; i++){
		//if(c&(3<<i*2)){
			unsigned char cd = (c&(3<<i*2))>>(i*2);
			d = d|((cd<<((3-i)*2)));
		//}
	}
	if(n%4 != 0){
		d = d<<((4-(n%4))*2);
	}

    return d;
}

unsigned char acces(size_t i, unsigned char c){
	return (c&(3<<(3-i)*2))>>((3-i)*2);
}

// 1100 0101 = 0101 0011
// 0000 0000
// 0000 0011
// 0000 0001
// 0100 0000
// 0000 1100
// 0000 0100
// 00 01 11 ..
// .. 11 01 00

//a0c3g2t1

//00 A
//01 T
//10 G
//11 C

//11000000

//char inverse(char c){
	// char tmp;
	// char pos_d;
	// char pos_f;
	// for(int i=0; i<4; i++){
	// 	// char in = c;
	// 	// in >>= ((3-getPosInByte(i))<<1);
	// 	// decode(in&3);
	// 	char in = c;
	// 	pos_d >>= ((3-getPosInByte(i))<<1);
	// 	pos_f <<= ((3-getPosInByte(4-i-1))<<1);
	// 	decode(in&3);
	// }
	
	// return decode(in&3);	
//}

/*char * decale(char * t, char octet_1, char octet_2, int nb_decale){
	char double_octet[2];
	double_octet[0] = octet_1;

	char &b = t[getByte(n)]; 
	size_t shift = ((3 - getPosInByte(n))<<1);
	b &= ~(3<<shift);
	b |= (encode(c) << shift);


	sequence_inv_comp.t[N-i-1] = sequence_inv_comp.t[i];
    sequence_inv_comp.t[i] = temp;
	if((i)%4==0)
		sequence_inv_comp.t[getByte(i+1)] = 0;
	cout << (unsigned char)sequence_inv_comp.t[getByte(i+1)] << " " << getByte(i+1) << " " << getPosInByte(i+1) << endl;
	cout << operator[](n-i) << endl;

	unsigned char cd = (encode(operator[](n-i)));
	cout << decode(cd) << " " << (int)cd << " " << (int) (cd<<((3-getPosInByte(i+1))*2)) << endl;
    sequence_inv_comp.t[getByte(i+1)] |= cd<<((3-getPosInByte(i+1))*2);
    printf("%d\n", (unsigned char)sequence_inv_comp.t[getByte(i+1)]);
    cout << decode(sequence_inv_comp[i]) << endl;

	return t;
}*/

EncodedSequence EncodedSequence :: RevComp() const {
	//EncodedSequence sequence_inv_comp(*this);
	EncodedSequence sequence_inv_comp(0);

    //char temp;
    //char nuc_begin_last;
    //char nuc_begin_first;

    //if( N == 1 ){
    //	sequence_inv_comp.t[0] = swich_acess(sequence_inv_comp.t[0], n);
    //}

    /*unsigned char d = 0;
	unsigned char cd = (c&(3<<i*2))>>(i*2);
	d = d|((cd<<((3-i)*2)));*/
    for(size_t i=0; i<n; i++){
    	//nuc_begin_last  = sequence_inv_comp[N-i-1];
    	//nuc_begin_first = sequence_inv_comp[i];
        //if ( nuc_begin_last != nuc_begin_first ) {//pour eviter des instructions atomique inuile
            // temp = nuc_begin_last;
            // nuc_begin_last  = ~(nuc_begin_first);
            // nuc_begin_first = ~(temp);
            //size_t shift = ((3 - getPosInByte(i))<<1);
            //sequence_inv_comp.t[N-i-1] = 'C';
            //sequence_inv_comp.t[i] = 'C';
            /*if(i==0)
    	   		sequence_inv_comp.t[N-i-1] = swich_acess(sequence_inv_comp.t[N-i-1], n);
    	   	else
    	   		sequence_inv_comp.t[N-i-1] = swich(sequence_inv_comp.t[N-i-1]);

    	   	sequence_inv_comp.t[i] = swich(sequence_inv_comp.t[i]);*/
            //cout << '\n' << sequence_inv_comp.t[N-i-1] << " " << sequence_inv_comp.t[i] << endl;
            //temp = sequence_inv_comp.t[N-i-1];

    		//--------------
            //sequence_inv_comp.t[N-i-1] = sequence_inv_comp.t[i];
    	    //sequence_inv_comp.t[i] = temp;
    		//if((i)%4==0)
    		//	sequence_inv_comp.t[getByte(i+1)] = 0;
    		//cout << (unsigned char)sequence_inv_comp.t[getByte(i+1)] << " " << getByte(i+1) << " " << getPosInByte(i+1) << endl;
    		//cout << operator[](n-i) << endl;

    		//unsigned char cd = (encode(operator[](n-i)));
    		//cout << decode(cd) << " " << (int)cd << " " << (int) (cd<<((3-getPosInByte(i+1))*2)) << endl;
    	    //sequence_inv_comp.t[getByte(i+1)] |= cd<<((3-getPosInByte(i+1))*2);
    	    //printf("%d\n", (unsigned char)sequence_inv_comp.t[getByte(i+1)]);
            //cout << decode(sequence_inv_comp[i]) << endl;

    	   	//cout << sequence_inv_comp.t[N-i-1] << " " << sequence_inv_comp.t[i] << endl;
    		sequence_inv_comp += decode(~(encode(operator[](n-i)))&3);
        //}
    }

	return sequence_inv_comp;
}

string EncodedSequence::subStr (int x, int y){
	string str("", 0);
	
	try{
		if( x+y > (int)n){
			throw "x+y trop grand";
		}
	}
	catch(const char * chaine){
		cerr << chaine << endl;
		throw " ";
	}


	for (int i = x; i < x+y; ++i){
		//cout << "deco " << operator[](i+1) << endl;
		str += operator[](i);
	}

	return str;
}

EncodedSequence * EncodedSequence::encoderChaine(const char * chaine){
	//cout << " enc " << endl;
	for (size_t i = 0; i < strlen(chaine); ++i)
	{
		setNucleotide(i+1, chaine[i]);
	}
	return this;
}

char * EncodedSequence::decoderChaine(){
	//cout << " dec " << endl;
	//string chaine("", n);
	char * chaine = new char[n+1];
	for (size_t i = 1; i <= n; ++i)
	{
		chaine[i-1] = operator[](i);
	}
	chaine[n] = '\0';
	return chaine;
}


int fmain(){
 	//0000 0001
 	//0100 1000
 	//128 64 32 16 8 4 2 1

 	//unsigned char c = 64+128;

 	//printf("%p  %d %d\n", c, (unsigned char)swich(c), acces(0, c));
 	//EncodedSequence e(0);
 	EncodedSequence * ee = new EncodedSequence(0);
 	EncodedSequence e = *ee;
 	//cout << e.getByte(0) << endl;

 	e.encoderChaine("TGCCAGTTCATTACTTCCTAAAACCAGCAGTTTATTGCCCAACACAGACAGAATGAATGTAGAAAAGGCTGAACTCTGCCATAAAAACAAACAGCCTGCCTTAGGAAGGAACCAAGAGAGCAGTCTCGATGAAAGTAAGGAAATATGTAGCCCTGGGAAGACCCCAGGTGCCTGTGAGCGGCATGAGCTGAATGACCATCATCTGTGTGAGAGGCAAGGCCTAGAGGAGAAGCCGCAGCACCCTGAGAGCCCTAGAGGTAATCCTCAGAACTGCCTGTCTGGAACCAAACTGAAAAGCAGCATTCAGAAAGTTAATGAGTGGTTATCCAGAAGTAGTGACATTTTAGCCTCTGATAACTCCAACGGTAGGAGCCATGAGCAGAGCGCAGAGGTGCCTAGTGCCTTAGAAGATGGGCATCCAGATACCGCAGAGGGAAATTCTAGCGTTTCTGAGAAGACTGACCCAGTGGCTGACAGCACTGATGATGCCTGGCTACATGTGCCTAAAAGAAGCTGTCTCAGGCCTGCAGAAAACAATATTGAAGATAAAATATTTGGAAAAACCTATCGGAGAAAGTCAGGTCACCCTAATTTGAATTATGTAACTGAAAACTTGTTTGCTGAAGCTGTGGCTCCTGATTCCTTGATCCCTCTAGAGGCTCCCAAAAACACCAGGTTAAAGCGTAAAGGAAGGAGCATAGCTGACCTGCAGCCTGAGGATTTCATCAGGAAGACGGACGTTCCAGTTATTCACAAAACCCCTGAAAAGAAAAATCACTCTGTTGACCAAATTCTCAAAAGAGAACAAAGTGACCAAGTGATGAACACGGCTAACAGTCTTCCTGAGCAGAAAGCCCTAGGTGGTCATGTAGGGGAAGTGAAAGATGTTCAGGTATTAGAGCTGTTCTCTGCGGAGAAAGAATCCACTTTCAGAACTGGAACAGAGCCTGTAGCTGGCAGCACAAACCATGGGGAGCTTGAATTAAATAGTAGAAATGCCAAAATGACAAGAAAAGACAGGCTGAGGAAGAAGCCTTCAGCCAGGATCGTCCGTGCTCTTGAGCTCGTAGTTGATAGAAACCCAAGCTCTTCTAATGAGAGTGAACTGCAGATCGATAGCTATCCCAGCAGTGAAGAGATAAGGAAAGGAACTAATTCTGAACAGAAGCAAATCAGACGCAGCAGAAGGCTTCGGCTGCTATCAGAAGAAATTGTGGTTGGAGCCAAGAAGGCCCATAAGCCAGATGACCAAGCAGAAGAAAGTTGTATCAGTGAAGTTTTCCCAGAACTAAAAATAGGAAACGTGCCTGCCTGTGCTACTGACAGTCTAACTACTGATAGGGATCAAGTGTTAGCTAGCTGCAGTTTCACAGAAGAAAGAGATGAAAGGAGCCTGGAAGCAATCCCAAGCAGCAAAGACCAAGATCTGCCCTTGAATGGAGGGGAGGGGTTGCAAGGTGAAAGAGCCCCAGGAAGCCTGGAGGCTTTGGAGGTTCCTGATACTGATTGGGACACTCAGGACAGTACCTCATTGTTTCCAGCTGATACTCCCCAAAATTCAAAAGCAGGACCCAGTCCTCACAGAAGCCACAGTGAAATAATGGAAACCCCCAAGGAACTCTTAGATGGTTGTTCATCCAAAAACACTGAAAGTGACGAAGAAGATTTGAGGAGCCTGATGAGACAGGAAGTTAAAAATGCCTCCAAGACAACCACAGAAATGGAGGATAGTGAACTCGACACCCAGTATTTACAAAATACCTTCAAACGTTCAAAGCGCCAGTCATTTGCTCTGCGTTCTAGCCCAAGGCAGGAATGTAGGAAACCCTCTGCTGTCCCTGGGACTGTAAATCAGCAGAGTCCAGATAACACCACAGATTGTGGGGGCCAGGAAAAAGAAAAGCAGGGAAACAGAGAATCAAACAAGCCTGTGTGGCCAAAGTCTGCAGTCATGAGCTTAGCTGCGGCTTGTCAGACAGAGGAGAGGAAGCCAGGTGTTTATGCCAAATGTAGCACCACAGAAGTGTCCAGGCTTTGTCACATAGCTCCATTACATGGTGTCATTGACTGTGAACACATTGCTGGAAACAACCAGGGAATTTCGCAAGTTCCTGATCAAAAACCATCAGTTTCTCCAGCAAGGTCATCTGCTAGTAAAACTATAAATACAAAAAACCTCCTGGAGGAAAGGCTTGATGAACAGACCACATGTCCTGAAACAGCTATGGGAAATGAAAGCTTAGCCCAAAGCAGCTTAAGTCCAGTAAGCCAAAGTAATAGCAGAGAATATATTTCTAAAGCAACTGACTTAAATAGATTTATCAGCATAGGCTCTAGTGGAGAAGGCAGTCAGGCACAAAAAGGTGAAAACAAAGAATCTGAATTAAATACACAACCCAAATTAAAGCTTGTGCAACCCCAAATCTGTCAACAAAGCTTTCCTCAGGATAATTGCAAAGAGTCTAAAAGAAAAGGGAAGGGAGGAAATGGAAAATTAGCTCAGGCCATCAGTACAGATTCATCTCCATGTTTAGAACAAACTAAAGAGAGTACACATTCTTCTCAGGTTTGTTCTGAGACACCTGATGACCT");
 	//cout << e[0] << " - " << e[2591] << endl;
 	//e.toStream(cout);
 	EncodedSequence r = e.RevComp();
 	//r.toStream(cout);
 	//cout << r[0] << " - " << r[4] << endl;
 	//cout <<"substtt " << e.subStr(1, 3) << endl;
// 	EncodedSequence e(1);
// 	e.setNucleotide(1, 'A');
// 	e.setNucleotide(2, 'T');
// 	e.setNucleotide(3, 'C');
// 	e.toStream(cout);

// 	cout << "   e[1]" << endl;
// 	cout << e[1] << endl;
// 	cout << (int)encode(decode(1)) << endl;
// 	cout << decode(1) << endl;
// 	cout << decode(encode('T')) << endl;
// 	cout << (int)encode('T') << endl;
// 	cout << e.getPosInByte(8) << endl;

// 	//EncodedSequence es(e);

// 	char * str = "aabbc";
// 	cout << &str[0] << " " << str[1] << endl;
// 	printf("%p %p %p %p %p %p\n", str, &str[0], &str[1], &str[2], &str[3], &str[4] );


	
// 	/*std::string str ("ATCG QDQD QSD SDS");
// 	std::cout << "str " << str << endl;
// 	string str2("",(int)str.size());
// 	int ind = 0;
// 	for(int i=0; i<str.size(); i++){
// 		if(str[i] != ' '){

// 			str2[ind] = str[i];
// 			std::cout << "str " << str[i] << str[ind] << endl;
// 			ind++;

// 		}
// 	}
// 	//a resize
// 	std::cout << "str " << str2 << endl;*/

// 	// int *p = (int*) malloc(sizeof(int)*10);
// 	// for(int i=0; i<10; i++){
// 	// 	p[i] = i;
// 	// }

// 	// for(int i=0; i<10; i++){
// 	// 	cout << p[i] << endl;
// 	// }


// 	//cout << (mrand48()&3) << endl;
 	return 0;

 }


// pour codage a 3bits on change 3 par 1 et on decale de 2 au lieu de 1


















