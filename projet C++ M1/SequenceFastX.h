#ifndef DEF_SEQUENCE_FAST_X
#define DEF_SEQUENCE_FAST_X
#include <string>
#include "EncodedSequence.h"


using namespace std;

class SequenceFastX{
    protected :

    //Attributs
    size_t taille;
    string entete;
    //string seq;
    EncodedSequence * seq_enc;
    size_t position;

    public :
    SequenceFastX(string entete="");
    SequenceFastX(string entete, size_t taille, string seq);
    SequenceFastX(string entete, size_t taille, EncodedSequence * seq_enc);
    virtual ~SequenceFastX();

    //Setter
    void setEntete(string entete);
    void setTaille(size_t taille);
    void setPosition(size_t position);
    //void setSequence(string seq);

    //Getter
    string getEntete() const;
    virtual char * getSequence() = 0;
    size_t getTaille() const;
    string getSequence() const;
    EncodedSequence * getSequenceEncoder() const;


    //Methode
    char complement_cara(char carac);
    string sequence_complementaire();
    string sequence_inverse();
    string sequence_reverse_complement();

};

#endif