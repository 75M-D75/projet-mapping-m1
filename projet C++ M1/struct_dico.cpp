#include "struct_dico.h"

Arbre::Node::Node(char c):
pere(NULL), 
fils(0), 
caractere(c),
num(0) 
{
}

Arbre::Node::Node(const Arbre::Node &n):
pere(NULL), 
fils(n.fils.size(), 0), 
caractere(n.caractere),
num(n.num) 
{
	for (size_t i = 0; i < fils.size(); ++i) {
		fils[i] = new Node(*n.fils[i]);
		fils[i]->pere = this;
	}
}

Arbre::Node::~Node() {
	for(size_t i = 0; i < fils.size(); ++i) {
		fils[i]->pere = NULL;
		delete fils[i];
	}
	if (pere) {
		pere->enleveFils(*this);
	}
}

Arbre::Node &Arbre::Node::operator=(const Arbre::Node &n) {
	if (this != &n) {
		for(size_t i = 0; i < fils.size(); ++i) {
			fils[i]->pere = NULL;
			delete fils[i];
		}
		for (size_t i = 0; i < fils.size(); ++i) {
			fils[i] = new Node(*n.fils[i]);
			fils[i]->pere = this;
		}
	}
	return *this;
}

void Arbre::Node::setPere(Arbre::Node *p) {
	if (!pere || (pere != p)) {
		if (pere) {
			pere -> enleveFils(*this);
		}

		if (p) {
			//p -> push_back(this);
		}
		pere = p;
	}
}


void Arbre::Node::enleveFils(Arbre::Node &f) {
	vector<Node *>::iterator it = find(fils.begin(), fils.end(), &f);
	if (it != fils.end()) {
		fils.erase(it);
		f.pere = NULL;
	}
}
	

void Arbre::Node::ajouteFils(Arbre::Node &f){
	vector<Node *>::iterator it = find(fils.begin(), fils.end(), &f);
	if (it == fils.end()) {
		fils.push_back(&f);
		f.pere = this;
	}
}

size_t Arbre::Node::getNbFils(){
	return nb_fils;
}

Arbre::Node *Arbre::Node::chercheCaractere(char c) const {
	bool existe = false;
	size_t i = 0;
	while ((i < fils.size()) && !existe){
		//cout << "caractere - " << fils[i]->caractere << "  : " << c << endl;
		if(fils[i]->caractere == c){
			existe = true;
			//cout << "@ " << this << " " << this -> fils[i] << endl;
		} 
		else {
			++i;
		}
	}
	return (existe ? fils[i] : NULL);
}

Arbre::Node *Arbre::Node::ajouteCaractere(char c) {
	Node * tmp = chercheCaractere(c);
	if (!tmp) {
		tmp = new Node(c);
		ajouteFils(*tmp);
		//tmp -> num = num;

	}
	return tmp;
}



Arbre::Arbre():
racine(),
num(0) {
}

void Arbre::Node::toStream(ostream &os) const{

	//os << " Node ";
	if(fils.empty()){
		os << " NULL " << endl;
		return;
	}

	for (size_t i = 0; i < fils.size(); ++i){
		os << fils[i] -> caractere << " [i : " << i << "]" ;
		os << " num : " << fils[i] -> num << " --> ";
		fils[i] -> toStream(os);
		cout << "\n";
	}
	cout << "\n";

}

void Arbre::toStream(ostream &os) const {
	racine.toStream(os);
}

ostream &operator<<(ostream &os, const Arbre &a) {
	a.toStream(os);
	return os;
}

Arbre &Arbre::addChaine(const string chaine) {
	Node * courant = &racine;
	size_t size = chaine.size();

	for (size_t e = 0; e < size; ++e){
		char c = chaine[e];
		courant = courant -> ajouteCaractere(c);
	}

	if(courant -> num == 0){

		num++;
		//cout << "nummm " << num << " " << chaine << endl;
		courant -> num = num;
		//courant -> num = num;
	}
	
	return *this;
}



Arbre::Node * Arbre::addChar(char c, Node * node){

	bool existe = false;

	Node * courant = node;

	for (size_t i = 0; i < courant -> getNbFils(); ++i){
		if( courant -> fils[i] -> caractere == c ){
			existe = true;
			courant -> fils[i] = addChar(c, (courant -> fils[i]));
		}
	}

	if( !existe ){
		//courant = addCaractere(courant, c);
		courant = addChar(c, (courant));
	}


	return courant;
}


Arbre::Node * Arbre::getRacine(){

	return &racine;
}

//FONCTION
void naive(string T, size_t n, char ** P, size_t m){
	Arbre arbre;
	for (size_t i = 0; i < m; ++i){
		arbre.addChaine(P[i]);
	}


	Arbre::Node * v;
	size_t j;

	for (size_t pos = 1; pos <= n; ++pos){
		v = arbre.getRacine();
		j = pos;
		while( v -> chercheCaractere(T[j]) != NULL ){
			if( v -> num != 0 ){
				cout << P[v -> num-1] << " apparait a la position : " << pos << endl;
			}

			v = v -> chercheCaractere(T[j]);
			j++;
		}
	}
}


vector<vector<size_t>>& naive_tab(string T, size_t n, char ** P, const size_t m, vector<vector<size_t>> &tab){
	Arbre arbre;
	if(tab.size() == 0){
		tab.resize(m);
	}

	for (size_t i = 0; i < m; ++i){
		cout << "add : " << P[i] << endl;
		arbre.addChaine(P[i]);

	}

	Arbre::Node * v;
	size_t j;

	for (size_t pos = 1; pos <= n; ++pos){
		v = arbre.getRacine();
		j = pos;
		while( v -> chercheCaractere(T[j]) != NULL ){
			if( v -> num != 0 ){
				cout << P[v -> num-1] << " apparait a la position : " << pos << endl;
				tab[v -> num-1].push_back(pos);
			}

			v = v -> chercheCaractere(T[j]);
			j++;
		}
	}

	//arbre.toStream(cout);
	return tab;
}

vector<vector<size_t>>& naive_tab_k_mer(string T, const string k_mer, string (*pointeurFonction)(string, size_t, size_t), const size_t k, vector<vector<size_t>> &tab){
	Arbre arbre;
	if(tab.size() == 0){
		tab.resize(k_mer.size()-k);
	}
	// cout << k_mer << endl;
	for (size_t i = 0; i < k_mer.size()-k; ++i){
		// cout << k_mer.substr(i, k) << endl;
		// for (size_t e = 0; e < i+1; ++e){
		// 	cout<<" ";
		// }
		arbre.addChaine(k_mer.substr(i, k));
	}
	// cout << k_mer.substr(k_mer.size()-k, k) << endl;
	// cout << "\n";

	Arbre::Node * v;
	size_t j;

	for (size_t pos = 1; pos < T.size(); ++pos){
		v = arbre.getRacine();
		j = pos;
		while( v -> chercheCaractere(T[j]) != NULL ){
			if( v -> num != 0 ){
				cout << (*pointeurFonction)(k_mer, k, v -> num-1) << " apparait a la position : " << pos << endl;
				tab[v -> num-1].push_back(pos);
			}

			v = v -> chercheCaractere(T[j]);
			j++;
		}
	}

	//arbre.toStream(cout);
	return tab;
}

vector<vector<size_t>>& naive_tab_k_mer(string T, const string k_mer, const size_t k, vector<vector<size_t>> &tab){
	vector<string> bloc_k_mer(0);
	size_t num_pred = 0;
	if(tab.size() == 0){
		tab.resize(k_mer.size()-k);
		//cout << tab.size() << " --- " << endl;
	}

	Arbre arbre;
	cout << k_mer << endl;
	for (size_t i = 0; i < k_mer.size()-k; ++i){
		cout << k_mer.substr(i, k) << endl;
		for (size_t e = 0; e < i+1; ++e){
			cout<<" ";
		}

		arbre.addChaine(k_mer.substr(i, k));

		if(arbre.num > num_pred){
			bloc_k_mer.push_back(k_mer.substr(i, k));
			num_pred = arbre.num;
			//cout << " num_pred " << num_pred;
		}
	}
	cout << k_mer.substr(k_mer.size()-k, k) << endl;
	cout << "\n";

	Arbre::Node * v;
	size_t j;

	for (size_t pos = 1; pos < T.size(); ++pos){
		v = arbre.getRacine();
		j = pos;
		while( (v = v -> chercheCaractere(T[j])) != NULL ){
			if( v -> num != 0 ){
				cout << bloc_k_mer[v -> num-1] << " apparait a la position : " << pos << endl;
				tab[v -> num-1].push_back(pos);
			}

			//v = v -> chercheCaractere(T[j]);
			j++;
		}
	}

	arbre.toStream(cout);
	return tab;
}

vector<string> naive_tab_k_mer_s(string T, const string k_mer, const size_t k, vector<vector<size_t>> &tab){
	vector<string> bloc_k_mer(0);
	size_t num_pred = 0;
	if(tab.size() == 0){
		tab.resize(k_mer.size()-k);
		//cout << tab.size() << " --- " << endl;
	}

	Arbre arbre;
	cout << "Lecture : \n" << k_mer << endl;
	cout << k << "-mers :\n";
	for (size_t i = 0; i < k_mer.size()-k; ++i){
		// cout << k_mer.substr(i, k) << endl;
		// for (size_t e = 0; e < i+1; ++e){
		// 	cout<<" ";
		// }

		arbre.addChaine(k_mer.substr(i, k));
		if(arbre.num > num_pred){
			bloc_k_mer.push_back(k_mer.substr(i, k));
			num_pred = arbre.num;
			//cout << " num_pred " << num_pred;
			cout << k_mer.substr(i, k) << " ";
		}
	}
	// cout << k_mer.substr(k_mer.size()-k, k) << endl;
	cout << "\n";

	Arbre::Node * v;
	size_t j;

	for (size_t pos = 1; pos < T.size(); ++pos){
		v = arbre.getRacine();
		j = pos;
		while( (v = v -> chercheCaractere(T[j])) != NULL ){
			if( v -> num != 0 ){
				//cout << bloc_k_mer[v -> num-1] << " apparait a la position : " << pos << endl;
				tab[v -> num-1].push_back(pos);
			}

			//v = v -> chercheCaractere(T[j]);
			j++;
		}
	}

	//arbre.toStream(cout);
	return bloc_k_mer;
}


vector<string> naive_tab_k_mer_s_last(string T, const string k_mer, const size_t k, vector<vector<size_t>> &tab, size_t &last){
	vector<string> bloc_k_mer(0);
	size_t num_pred = 0;
	if(tab.size() == 0){
		tab.resize(k_mer.size()-k);
		//cout << tab.size() << " --- " << endl;
	}

	Arbre arbre;
	cout << "Lecture : \n" << k_mer << endl;
	cout << k << "-mers :\n";
	for (size_t i = 0; i < k_mer.size()-k; ++i){
		// cout << k_mer.substr(i, k) << endl;
		// for (size_t e = 0; e < i+1; ++e){
		// 	cout<<" ";
		// }

		arbre.addChaine(k_mer.substr(i, k));
		if(arbre.num > num_pred){
			bloc_k_mer.push_back(k_mer.substr(i, k));
			num_pred = arbre.num;
			//cout << " num_pred " << num_pred;
			cout << k_mer.substr(i, k) << " ";
		}
	}
	// cout << k_mer.substr(k_mer.size()-k, k) << endl;
	cout << "\n";

	Arbre::Node * v;
	size_t j;

	for (size_t pos = 1; pos < T.size(); ++pos){
		v = arbre.getRacine();
		j = pos;
		while( (v = v -> chercheCaractere(T[j])) != NULL ){
			if( v -> num != 0 ){
				//cout << bloc_k_mer[v -> num-1] << " apparait a la position : " << pos << endl;
				tab[v -> num-1].push_back(pos);
				last = pos;
			}

			//v = v -> chercheCaractere(T[j]);
			j++;
		}
	}

	//arbre.toStream(cout);
	return bloc_k_mer;
}


vector<string> naive_tab_k_mer_s_last_enc(EncodedSequence & T, EncodedSequence & k_mer, const size_t k, vector<vector<size_t>> &tab, size_t &last){
	vector<string> bloc_k_mer(0);
	size_t num_pred = 0;
	if(tab.size() == 0){
		tab.resize(k_mer.getSize()-k);
		//cout << tab.size() << " --- " << endl;
	}

	Arbre arbre;
	//cout << "Lecture : \n";
	//k_mer.toStream(cout);
	//cout << k << "-mers :\n";
	for (size_t i = 1; i <= k_mer.getSize()-k; ++i){
		// cout << k_mer.substr(i, k) << endl;
		// for (size_t e = 0; e < i+1; ++e){
		// 	cout<<" ";
		// }

		arbre.addChaine(k_mer.subStr(i, k));
		if(arbre.num > num_pred){
			bloc_k_mer.push_back(k_mer.subStr(i, k));
			num_pred = arbre.num;
			//cout << " num_pred " << num_pred;
			//cout << k_mer.subStr(i, k) << " ";
		}
	}
	// cout << k_mer.substr(k_mer.size()-k, k) << endl;
	//cout << "\n";

	Arbre::Node * v;
	size_t j;

	for (size_t pos = 1; pos < T.getSize(); ++pos){
		v = arbre.getRacine();
		j = pos;
		//cout << T[j] << endl;
		while( (v = v -> chercheCaractere(T[j])) != NULL && j < T.getSize() ){
			if( v -> num != 0 ){
				//cout << bloc_k_mer[v -> num-1] << " apparait a la position : " << pos << endl;
				tab[v -> num-1].push_back(pos);
				last = pos;
			}

			//v = v -> chercheCaractere(T[j]);
			j++;
		}
	}

	//arbre.toStream(cout);
	return bloc_k_mer;
}


vector<string> naive_tab_k_mer_s_last_enc_rev_comp(EncodedSequence & T, EncodedSequence & k_mer, const size_t k, vector<vector<size_t>> &tab, size_t &last){
	vector<string> bloc_k_mer(0);
	size_t num_pred = 0;
	if(tab.size() == 0){
		tab.resize(k_mer.getSize()-k);
		//cout << tab.size() << " --- " << endl;
	}

	Arbre arbre;
	//cout << "Lecture : \n";
	//k_mer.toStream(cout);
	//cout << k << "-mers :\n";
	char * chaine = NULL;
	for (size_t i = 1; i <= k_mer.getSize()-k; ++i){
		// cout << k_mer.substr(i, k) << endl;
		// for (size_t e = 0; e < i+1; ++e){
		// 	cout<<" ";
		// }

		EncodedSequence rev_comp(k);
		rev_comp.encoderChaine(k_mer.subStr(i, k).c_str());
		rev_comp = rev_comp.RevComp();
		//cout << "reeeeeeeevvv ";
		//rev_comp.toStream(cout);
		chaine = rev_comp.decoderChaine();

		arbre.addChaine(chaine);
		if(arbre.num > num_pred){
			bloc_k_mer.push_back(chaine);
			num_pred = arbre.num;
			//cout << " num_pred " << num_pred;
			//cout << chaine << " ";
			
		}
		delete [] chaine;
		chaine = NULL;
	}
	// cout << k_mer.substr(k_mer.size()-k, k) << endl;
	//cout << "\n";

	Arbre::Node * v;
	size_t j;

	for (size_t pos = 1; pos < T.getSize(); ++pos){
		v = arbre.getRacine();
		j = pos;
		while( (v = v -> chercheCaractere(T[j])) != NULL && j < T.getSize() ){
			if( v -> num != 0 ){
				//cout << bloc_k_mer[v -> num-1] << " apparait a la position : " << pos << endl;
				tab[v -> num-1].push_back(pos);
				last = pos;
			}

			//v = v -> chercheCaractere(T[j]);
			j++;
		}
	}

	//arbre.toStream(cout);
	return bloc_k_mer;
}


// int rmain(){

// 	Arbre arbre;
// 	Arbre::Node * r = arbre.getRacine();

// 	cout << "r " << r -> nb_fils  << endl;
// 	arbre = arbre.addChaine("Bonjour");
// 	r = arbre.getRacine();
// 	cout << "r " << r -> nb_fils  << endl;
// 	arbre = arbre.addChaine("Salut");
// 	arbre = arbre.addChaine("Salle");
	
// 	arbre.toStream(cout);
// 	char * tab[3] = {"ATG", "ATTC", "GCC"};
// 	vector<vector<size_t>> tab_s(3);

// 	naive("#ATGCCCCATG", 10, tab, 3);
// 	string T("ATGCCCCATG");

// 	tab_s = naive_tab("#"+T, 10, tab, 3, tab_s);

// 	for (int i = 0; i < 3; ++i)
// 	{
// 		for (size_t e = 0; e < tab_s[i].size(); ++e)
// 		{
// 			cout << " tab s : " << tab_s[i][e] << " " << tab[i] << endl;

// 		}
// 	}

// 	return 0;
// }