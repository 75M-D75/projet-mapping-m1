#include "SequenceFastq.h"
#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fstream>


SequenceFastq::SequenceFastq(string entete){
	this -> entete = entete;
	this -> taille = 0;
}

SequenceFastq::SequenceFastq(string entete, size_t taille, string seq, size_t quality){
	this -> taille  = taille;
	this -> entete  = entete;
	this -> quality = quality;

	this -> seq_enc = new EncodedSequence(0);
    seq_enc -> encoderChaine(seq.c_str());
}

SequenceFastq::SequenceFastq(string entete, size_t taille, EncodedSequence * seq_enc, size_t quality){
	this -> taille  = taille;
	this -> entete  = entete;
	this -> seq_enc = seq_enc;
	this -> quality = quality;
}

SequenceFastq::SequenceFastq(SequenceFastq & sequence){
	this -> quality = sequence.getQuality();
	this -> taille  = sequence.getTaille();
	this -> entete  = sequence.getEntete();
	//this -> seq_enc = sequence.getSequenceEncoder();
	this -> seq_enc = new EncodedSequence(0);
	//sequence.getSequenceEncoder() -> toStream(cout);
	char * chaine = sequence.getSequenceEncoder() -> decoderChaine();
    seq_enc -> encoderChaine(chaine);
    delete [] chaine;
}



SequenceFastq::~SequenceFastq(){
	//cout << "Destructor SequenceFastq" << endl;
	if (seq_enc)
	{
		/* code */
		delete seq_enc;
	}
}

//Setter
void SequenceFastq::setQuality(float quality){
	this -> quality = quality;
}

//Getter
float  SequenceFastq::getQuality() const{
	return quality;
}

char * SequenceFastq::getSequence(){
	return seq_enc -> decoderChaine();
}