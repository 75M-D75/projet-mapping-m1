#ifndef DEF_SEQUENCE_FASTQ
#define DEF_SEQUENCE_FASTQ
#include <string>
#include "SequenceFastX.h"


using namespace std;

class SequenceFastq : public SequenceFastX{
    private :

    //Attributs
    float quality;

    public :
    SequenceFastq(string entete="");
    SequenceFastq(SequenceFastq & sequence);
    SequenceFastq(string entete, size_t taille, string seq, size_t quality);
    SequenceFastq(string entete, size_t taille, EncodedSequence * seq_enc, size_t quality);
    virtual ~SequenceFastq();

    //Setter
    void setQuality(float quality);

    //Getter
    float  getQuality() const;
    char * getSequence();

    //Methode
    char complement_cara(char carac);
    string sequence_complementaire();
    string sequence_inverse();

};

#endif