#include <string>
#include "SequenceFastX.h"
#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fstream>


SequenceFastX::SequenceFastX(string entete):taille(0) {
    //cout << "Fastaaa 7" << endl;
    this -> entete  = entete;
    this -> seq_enc = NULL;
}

SequenceFastX::SequenceFastX(string entete, size_t taille, string seq){
    //cout << "Fastaaa 4" << endl;
    this -> taille = taille;
    this -> entete = entete;
    //this -> seq    = seq;
    this -> seq_enc = new EncodedSequence(0);
    seq_enc -> encoderChaine(seq.c_str());
}

SequenceFastX::SequenceFastX(string entete, size_t taille, EncodedSequence * seq_enc){
    //cout << "FastX 2" << endl;
    this -> taille  = taille;
    this -> entete  = entete;
    this -> seq_enc = seq_enc;
}

SequenceFastX::~SequenceFastX(){
    //cout << "Destructor SequenceFastX virtual" << endl;
}

//Setter
void SequenceFastX::setEntete(string entete){
    this -> entete = entete;
}

//
void SequenceFastX::setTaille(size_t taille){
    this -> taille = taille;
}

//
void SequenceFastX::setPosition(size_t position){
    this -> position = position;
}

//
// void SequenceFastX::setSequence(string seq){
//     this -> seq = seq;
// }


//Getter
string SequenceFastX::getEntete() const {
    return entete;
}

size_t SequenceFastX::getTaille() const {
    return taille;
}

EncodedSequence * SequenceFastX::getSequenceEncoder() const{
    return seq_enc;
}



//Methodes
char SequenceFastX::complement_cara(char carac){
    switch(carac){
        case 'A':
            return 'T';
        case 'T':
            return 'A';
        case 'G':
            return 'C';
        case 'C':
            return 'G';
        case 'Y':
            return 'R';
        case 'K':
            return 'M';
        case 'M':
            return 'K';
        case 'B':
            return 'V';
        case 'V':
            return 'B';
        case 'D':
            return 'H';
        case 'H':
            return 'D';
        case 'N':
            return 'N';
        case 'U':
            return 'A';
        default:
            return ' ';
    }
}

string SequenceFastX::sequence_complementaire(){
    //string sequence_comp = this -> seq;
    char * sequence_comp = this -> seq_enc -> decoderChaine();
    //int n = sequence_comp.size();
    int n = strlen(sequence_comp);

    for(int i=0; i<n; i++){
        sequence_comp[i] = complement_cara(sequence_comp[i]);
    }

    string s(sequence_comp);
    delete [] sequence_comp;

    return s;
}


string SequenceFastX::sequence_inverse(){
    //string sequence_inverse = this -> seq;
    char * sequence_inverse = this -> seq_enc -> decoderChaine();
    //int n = sequence_inverse.size();
    int n = strlen(sequence_inverse);
    char temp;

    for(int i=0; i<n/2; i++){
        if ( sequence_inverse[n-i-1] != sequence_inverse[i] ) {//pour eviter des instructions atomique inuile

            temp = sequence_inverse[n-i-1];
            sequence_inverse[n-i-1] = sequence_inverse[i];
            sequence_inverse[i] = temp;
        }
    }    

    string s(sequence_inverse);
    delete [] sequence_inverse;

    return s;
}


string SequenceFastX::sequence_reverse_complement(){
    //string sequence_inv_comp = this -> seq;
    char * sequence_inv_comp = (char *)this -> seq_enc -> decoderChaine();
    //int n = sequence_inv_comp.size();
    int n = strlen(sequence_inv_comp);
    char temp;

    for(int i=0; i<n/2; i++){
        if ( sequence_inv_comp[n-i-1] != complement_cara(sequence_inv_comp[i]) ) {//pour eviter des instructions atomique inuile
            temp = sequence_inv_comp[n-i-1];
            sequence_inv_comp[n-i-1] = complement_cara(sequence_inv_comp[i]);
            sequence_inv_comp[i] = complement_cara(temp);
        }
    }    

    return sequence_inv_comp;
}
