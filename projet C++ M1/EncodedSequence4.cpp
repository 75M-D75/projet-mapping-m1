#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "EncodedSequence4.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> /* close */
#include <netdb.h>  /* gethostbyname */
#include <sys/ipc.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <string.h>
#include <fstream>
#include <string>
#include <vector>
#include <regex>

#include <stdio.h>
#include <string.h>
#include <string>

#include <cstdio>

#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fstream>
#include <vector>
#include <regex>

//Constructeurs
EncodedSequence :: EncodedSequence(size_t n) :
	n(0), 
	N(n? getByte(n)+1:0), 
	t(N ? new char [N] :NULL) 
{
	cout << "Constructeurs EncodedSequence " << endl;
}

EncodedSequence :: EncodedSequence (const EncodedSequence &es):
	n(es.n),
	N(es.N),
	t(N ? new char[N]:NULL) 
{

 	for (size_t i=0; i<N; ++i)
	{
		t[i] = es.t[i];
		copy(es.t, es.t+N, t);
	}
}


EncodedSequence :: ~EncodedSequence()
{
	cout << "Destructor EncodedSequence" << endl;
	clear(); 
}

//Methide
EncodedSequence &EncodedSequence :: operator= (const EncodedSequence &es)
{
	if(this != &es)
	{
		clear();
		n=es.n;
		N = es.N;
		t= n? new char [N] :NULL;
		copy(es.t, es.t + N,t);
	}
	return *this;
}


void EncodedSequence :: clear(){
	if(N){
		delete []t;
		t=NULL;
		N=0;
		n=0; 
	}
}

void EncodedSequence :: reserve( size_t n){
	size_t N = getByte(n)+1;
	if(N > this -> N){
		char *t = new char [N];
		copy(this -> t, this -> t + this -> N, t);
		delete [] this -> t;
		this -> t = t;          
		this -> N = N;
	}
}


size_t EncodedSequence :: getByte(size_t i)
{
	return i? (i-1)>>1 : 0;
}



size_t EncodedSequence :: getPosInByte (size_t i){
	//return i? (i-1)&1 :0;
	return i? (i-1)%2 :0;
}

//Local methode
char encode(char c){
	return (((c=='a') || (c=='A')) ? 0:
		(((c=='c')||(c=='C')) ? 14:
		 	(((c=='g')||(c=='G')) ? 1:
		 		(((c=='t')|| (c=='T')||(c=='u')|| (c=='U')) ? 15:
					(((c=='b')||(c=='B')) ? 2:
						(((c=='d')||(c=='D')) ? 3:
							(((c=='h')||(c=='H')) ? 12:
								(((c=='v')||(c=='V')) ? 13:
									(((c=='r')||(c=='R')) ? 4:
										(((c=='y')||(c=='Y')) ? 11:
											(((c=='w')||(c=='W')) ? 5:
												(((c=='s')||(c=='S')) ? 10:
													(((c=='m')||(c=='M')) ? 6:
														(((c=='k')||(c=='K')) ? 9:
															((c=='_')? 7:
																(((c=='n')||(c=='N')) ? 8:
																	(mrand48()%16)))))))))))))))));
}


char decode (char c){
	return ((c==0) ?'A':
	((c==15) ?'T':
		((c==1) ?'G':
			((c==14) ?'C':
				((c==2) ?'B':
					((c==3) ?'D':
						((c==12) ?'H':
							((c==13) ?'V':
								((c==4) ? 'R':
									((c==11) ? 'Y':
										((c==5) ? 'W':
											((c==10) ? 'S':
												((c==6) ? 'M':
													((c==9) ? 'K':
														((c==7) ? '_':
															 'N')))))))))))))));
}


// Operateur d'acces en lecture
char EncodedSequence :: operator[](size_t i) const 
{
	try{
		if (i>n){
			throw "out of bound operator[]";
		}
	}
	catch(const char * chaine){
		cerr << chaine << endl;
		throw " ";
	}
	
	char c = 'A';
	c = t[getByte(i)];
	c >>= ((1-getPosInByte(i))<<2);
	return decode(c&15);
}

//

//operateur d'écriture
void EncodedSequence :: setNucleotide(size_t i, char b){
	reserve(i); 
	if (i > n){
		n = i;
	}

	char &c = t[getByte(i)];

	size_t shift = ((1 - getPosInByte(i))<<2);
	c &= ~(15<<shift);
	c |= (encode(b) << shift);
}

size_t EncodedSequence :: getSize(){
	return n;
}

void EncodedSequence :: toStream( ostream &os ) const {
	for (size_t i=1; i<=n; ++i)
	{
		os << operator[](i);
	}
	os << endl;
}


EncodedSequence &EncodedSequence :: operator+=(char c){
	reserve(n+1);
	++n;
	char &b = t[getByte(n)]; 
	size_t shift = ((1 - getPosInByte(n))<<2);
	b &= ~(15<<shift);
	b |= (encode(c) << shift);
	return *this;
}


EncodedSequence EncodedSequence :: RevComp() const {
	EncodedSequence sequence_inv_comp(0);

    for(size_t i=0; i<n; i++){
		sequence_inv_comp += decode(~(encode(operator[](n-i)))&15);
    }

	return sequence_inv_comp;
}

EncodedSequence * EncodedSequence::encoderChaine(const char * chaine){
	for (size_t i = 0; i < strlen(chaine); ++i)
	{
		setNucleotide(i+1, chaine[i]);
	}
	return this;
}


char * EncodedSequence::decoderChaine(){
	char * chaine = new char[n+1];
	for (size_t i = 1; i <= n; ++i)
	{
		chaine[i-1] = operator[](i);
	}
	chaine[n] = '\0';
	return chaine;
}


int fmain(){
 	EncodedSequence * ee = new EncodedSequence(0);
 	EncodedSequence e = *ee;
 	//EncodedSequence e(0);
 	//ee = &e;

 	for (int i = 0; i < 8; ++i)
	{
		cout << e.getByte(i) << endl;
	}
	cout << "\n";

	for (int i = 0; i < 8; ++i)
	{
		cout << e.getPosInByte(i) << endl;
	}
	//string st;
	string st("CGATCCAGATACCGTTACTGTTCGCATCCCACTGCACAATGATACAGTCATCCTGTCCGACAATTTCCAGCCTCGCCGGTACAGCTGCCATGACAATAACCCGCCTCT");
 	//cin << "CGATCCAGATACCGTTACTGTTCGCATCCCACTGCACAATGATACAGTCATCCTGTCCGACAATTTCCAGCCTCGCCGGTACAGCTGCCATGACAATAACCCGCCTCT";
 	//getline(cin, st);
 	string sp = "";
 	sp += st;
 	//ee->encoderChaine("CGATCCAGATACCGTTACTGTTCGCATCCCACTGCACAATGATACAGTCATCCTGTCCGACAATTTCCAGCCTCGCCGGTACAGCTGCCATGACAATAACCCGCCTCT");
 	ee->encoderChaine(sp.c_str());
 	sp.clear();
 	cout << ee->decoderChaine() << endl;
 	/*cout << e[2] << " - " << e[3] << endl;
 	e.toStream(cout);
 	EncodedSequence r = e.RevComp();
 	r.toStream(cout);
 	cout << r[0] << " - " << r[4] << endl;*/

 	return 0;

}


















