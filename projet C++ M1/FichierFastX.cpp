#include "FichierFastX.h"
#include <stdio.h>
#include <string.h>
#include <string>

#include <cstdio>

#include "SequenceFastX.h"
#include "SequenceFasta.h"



//Constructeur
FichierFastX::FichierFastX(const char * file_name):
file_name((file_name?new char[strlen(file_name) + 1]:NULL)),
seq_pos(NULL),
nb_sequence(0),
format(0),
sequenceFastx(NULL) {

    // strcpy(this -> file_name, file_name);
    // cout << this -> file_name << endl;
    //if(file_name)
    setFileName(file_name);
}


FichierFastX::FichierFastX(FichierFastX &f):
file_name(new char[strlen(f.file_name) + 1]),
seq_pos(new size_t[f.nb_sequence]),
nb_sequence(f.nb_sequence),
format(f.format) {


    strcpy(this -> file_name, f.getFileName());
    for(size_t i=0; i<f.getNbSequence(); i++){
        if( f.getFormat() == 1 ){
            //SequenceFasta * seqFasta = (SequenceFasta *) f.seq_pos[i].seq;
            //seq_pos[i].seq = new SequenceFasta(*seqFasta);

        }
        else{
            //SequenceFastq * seqFastq = (SequenceFastq *) f.seq_pos[i].seq;
            //seq_pos[i].seq = new SequenceFasta(*seqFastq);
        }

        seq_pos[i] = f.seq_pos[i];
    }

    if(f.sequenceFastx){
        if(f.format == 1){
            sequenceFastx = new SequenceFasta(f.sequenceFastx);
        }
        else if(f.format == 2){
            SequenceFastq * sq = (SequenceFastq*) f.sequenceFastx;

            sequenceFastx = new SequenceFastq(*sq);
            //cout << "\n\n adr " << f.sequenceFastx << " " << sequenceFastx << " " << sq << endl;
        }
    }
    else{
        sequenceFastx = NULL;
    }

    
}



//Setter
void FichierFastX::setFileName(const char * file_name){

    if(this -> file_name){
        delete [] this -> file_name;

        if(seq_pos){
            delete [] seq_pos;
            seq_pos = NULL;
        }
    }

    if(file_name){
        this -> file_name = new char[strlen(file_name) + 1];

        strcpy(this -> file_name, file_name);
        nb_sequence = 0;

        indexation();
    }
    else{
        this -> file_name = NULL;
    }
}


const char * FichierFastX::getFileName() const {
    return file_name;
}


//Getter
size_t FichierFastX::getFormat() const {
    return format;
}


size_t FichierFastX::getNbSequence() const {
    return nb_sequence;
}


void FichierFastX::toStream (ostream &os) const {
    os << "File : " << (file_name ? file_name : "<no file >") << endl;
    os << "Nombre de sequences = " << nb_sequence << endl;
    //....Possibiliter de mettre plus
}


ostream &operator << (ostream &os, const FichierFastX &f ){
    f.toStream(os);
    return os;
}


//local
int getLigneSansSpace(ifstream &f, string &ligne){
    string str = "";
    ligne = "";
    //cout << " trace " << f.peek() << " " << f.peek() << endl;
    //cout << str << endl;

    if(f.peek() == -1)
        return 0;

    while( f && (int)f.peek() != -1 && f.peek() != '\n' ){
        if( !isspace(f.peek()) && f.peek() != ' '){
            str += f.get();
        }
        else
            f.get();
    }

    if(f.peek() != -1 && f.peek() == '\n')
        f.get();

    ligne = str;
    //cout << ligne << " ligne " << ligne.at(0) << endl;

    return 1;
}



size_t * FichierFastX::getSeqPos(){
    return seq_pos;
}

SequenceFastX * FichierFastX::getSequenceFastX(){
    return sequenceFastx;
}


SequenceFastX * FichierFastX::getSequence(size_t numero_sequence){

    try
    {
        if( numero_sequence > nb_sequence + 1 ){
            throw "sequence inexistante : ";
        }
    }
    catch(char const* chaine)
    {
       cerr << chaine << endl;
       //throw " ";
       return NULL;
    }

    if( sequenceFastx ){
        delete sequenceFastx;
        sequenceFastx = NULL;
    }

    EncodedSequence * en = new EncodedSequence(0);
    //EncodedSequence 
    //EncodedSequence ene(0);
    //EncodedSequence * en = &ene;

    size_t somme = 0;
    ifstream fichier(file_name);
    string entete;
    //string sequence = "";
    size_t size = 0;
    size_t format = 0;

    size_t position = (size_t)seq_pos[numero_sequence - 1];
    //SequenceFastX * sequenceFastx;

    //cout << position << " position " << numero_sequence << endl;

    if ( fichier ) {  
        string ligne;
        fichier.seekg(position);
        getline(fichier, ligne);
        entete = ligne;
        //sequenceFastx -> setEntete( ligne );
        //cout << "entete : " << ligne << endl;

        if( ligne[0] != '@' ){
            format = 1;
        }
        else{
            format = 2;
        }

        if( format == 1 ){
            //cout << " Format 1" << endl;
            // while( getLigneSansSpace(fichier, ligne) && ligne[0] != '>' && ligne[0] != ';' ){
            //     sequence = sequence + ligne;
            // }

            int indice_col = 0;
            while( fichier && (int)fichier.peek() != -1 && (( indice_col != 0 ) || (fichier.peek() != ';' && fichier.peek() != '>')) ){
                if( !isspace(fichier.peek()) && fichier.peek() != ' ' && fichier.peek() != '\n'){
                    (*en) += fichier.get();
                    indice_col++;
                }
                else{
                    if(fichier.get() == '\n'){
                        indice_col = 0;
                    }
                    else{
                        indice_col++;
                    }
                } 
            }

        }
        else{
            //cout << "Format 2" << endl;
            //fastq
            /*while( getLigneSansSpace(fichier, ligne) && ligne.at(0) != '+' ){
                sequence = sequence + ligne;
            } */  
            //cout << "\n\nF 2" << sequence <<endl;

            int indice_col = 0;
            while( fichier && (int)fichier.peek() != -1 && (( indice_col != 0 ) || (fichier.peek() != '+')) ){
                if( !isspace(fichier.peek()) && fichier.peek() != ' ' && fichier.peek() != '\n'){
                    (*en) += fichier.get();
                    indice_col++;
                }
                else{
                    if(fichier.get() == '\n'){
                        indice_col = 0;
                    }
                    else{
                        indice_col++;
                    }
                } 
            }

            if(fichier && (int)fichier.peek() != -1 && fichier.peek() == '+'){
                getline(fichier, ligne);
                size = ligne.size();
                for(size_t i=0; i<ligne.size(); i++){
                    somme += ligne[i];
                    //cout << somme << " ";
                }
            }

            // if(ligne.at(0) == '+'){
            //     getline(fichier, ligne);
            //     size = ligne.size();
            //     for(size_t i=0; i<ligne.size(); i++){
            //         somme += ligne[i];
            //         //cout << somme << " ";
            //     }
            // }
            

            while(size < (*en).getSize() && getline(fichier, ligne) ){
                size += ligne.size();
                for(size_t i=0; i<ligne.size(); i++){
                    somme += ligne[i];
                    //cout << somme << " ";
                }
            }
        }

        fichier.close();
        //TODO QQQ
        //cout << sequence.c_str()<<endl;
        //en -> encoderChaine(sequence.c_str());
        //en -> toStream(cout);

        //cout << '\n' << en -> decoderChaine() << "\n" << endl; 
        // ene. encoderChaine("CGATCCAGATACCGTTACTGTTCGCATCCCACTGCACAATGATACAGTCATCCTGTCCGACAATTTCCAGCCTCGCCGGTACAGCTGCCATGACAATAACCCGCCTCT");
        // ene. toStream(cout);
        
        // cout << '\n' << ene. decoderChaine() << "\n" << endl;
        //sequenceFastx = new SequenceFasta(entete, sequence.size(), sequence);

        //en -> encoderChaine(sequence.c_str());
        if( format == 1 )
            sequenceFastx = new SequenceFasta(entete, en -> getSize(), en);
        else{
            sequenceFastx = new SequenceFastq(entete, en -> getSize(), en, somme);
            //SequenceFastq s(entete, sequence.size(), en, somme);
            //sequenceFastx = &s;
        }
    }
    else{
        cerr << "Impossible d'ouvrir le fichier !  " << endl;
    }

    return sequenceFastx;
}

//Methode
FichierFastX & FichierFastX::operator=(FichierFastX &f){
    if( &f != this ){
        if( file_name ){
            //cout << "free 1" << endl;
            delete [] file_name;
            file_name = NULL;
            //cout << "free 1 end" << endl;
        }

        if( seq_pos ){
            //cout << "free 2" << endl;
            delete [] seq_pos;
            seq_pos = NULL;
        }

        if( sequenceFastx ){
            //cout << "free 3" << endl;
            delete sequenceFastx;
            sequenceFastx = NULL;
        }

        this -> file_name = new char[strlen(f.file_name) + 1];
        strcpy(this -> file_name, f.getFileName());

        seq_pos = new size_t[f.getNbSequence()];


        for(size_t i=0; i<f.getNbSequence(); i++){
            /*if( f.getFormat() == 1 )
                seq_pos[i].seq = new SequenceFasta(*(f.seq_pos[i].seq));*/

            seq_pos[i] = f.seq_pos[i];
        }

        nb_sequence = f.getNbSequence();

        if(f.sequenceFastx){
            if(f.format == 1){
                sequenceFastx = new SequenceFasta(f.sequenceFastx);
            }
            else if(f.format == 2){
                SequenceFastq * sq = (SequenceFastq*) f.sequenceFastx;
                sequenceFastx = new SequenceFastq(*sq);
            }
        }
        else{
            sequenceFastx = NULL;
        }
    }

    return *this;
}

string ligneSansSpace(string ligne){
    string line("");

    for (size_t i = 0; i < ligne.size(); ++i)
    {
        if(ligne[i] != '\n' && ligne[i] != ' '){
            line += ligne[i];
        }
    }
    //cout<<"\n\nsize "<< line.size() << endl;
    return line;
}

int FichierFastX::checkFormat(){

    //ifstream fichier(file_name, ios::in);
    std::ifstream fichier(file_name);
    bool matching;
    int  num_ligne = 0;
    int positionFastQ = 0;
    size_t taille_Seq = 0;
    size_t size = 0;

    if ( fichier ) {  
        string ligne = "";
        //cout << format << " " << endl;
        while( getline(fichier, ligne) && format == 0 ){
            if ( (ligne.at(0) == '>' || ligne.at(0) == ';' ) ){
                format = 1;//fasta
            }
            else if( ligne.at(0) == '@' ){
                format = 2;//fastq
            }
        }

        if( !format ){
            throw "Format fichier incorect";
        }

        fichier.clear();
        fichier.seekg(0);

        while( getline(fichier, ligne) ){

            if( ligne.size() > 0 ){

                if(format == 1){
                    //fasta
                    if( (ligne.at(0) == '>' || ligne.at(0) == ';' ) ){//ligne d'entête
                        nb_sequence++;
                    }
                    else if( ligne.at(0) != '>' && ligne.at(0) != ';' ){//Supposé être les lignes des sequences

                        if( !( ligne.length() <= 120 ) ){
                            cout << "Nombre de caractére trop elever sur la ligne : " << num_ligne << endl;
                            return -1;
                        }

                        regex expression {"[A-Z-]*"};//?

                        matching = regex_match(ligne, expression);

                        if ( !matching ) {//Mauvais caractére identifier
                            cout << "Format de fichier inexact erreur a la ligne : " << num_ligne << endl;
                            return -2;
                        }
                    }
                }
                else{
                    //fastq
                    if( ligne.at(0) == '@' && positionFastQ == 0 ){
                        nb_sequence++;
                        //cout << "er " << ligne.at(0) << endl;
                        if( (int)fichier.tellg() > (int)ligne.size() ){
                            taille_Seq = 0;
                        }
                    }
                    else{
                        if(ligne.at(0) == '+'){
                            positionFastQ = 1;
                            if( getline(fichier, ligne) ){
                                positionFastQ = 2;
                            }
                        }

                        if( positionFastQ == 0 ){
                            //taille_Seq += ligne.size();
                            taille_Seq += ligneSansSpace(ligne).size();
                        }
                        else if( positionFastQ == 2 ){
                            size = ligne.size();
                            while( size < taille_Seq && getline(fichier, ligne) ){
                                size += ligne.size();
                            }
                            positionFastQ = 0;
                        }
                    }
                }
            }
        }
        num_ligne++;  
        
        fichier.close();  
    }
    else{
        cerr << "Impossible d'ouvrir le fichier !" << file_name << endl;
        return -1;
    }

    return format;
}


const size_t * FichierFastX::indexation(){

    ifstream fichier(file_name);
    int num_ligne;
    int indice_seq = 0;
    size_t taille_Seq = 0;
    int positionFastQ = 0;
    size_t format = 0;
    //size_t * seq_pos;


    if ( fichier ) {  
        string ligne;

        if( nb_sequence == 0 )
            format = checkFormat();

        format = this -> format;

        if( !format ){
            throw "Format fichier incorect";
        }

        fichier.clear();
        fichier.seekg(0);

        //cout << " nb " << nb_sequence << endl;
        if(!seq_pos)
            seq_pos = new size_t[nb_sequence+1];
        //cout << &seq_pos << " adress " <<endl; 

        // size_t * new_seq_pos = new size_t[nb_sequence+1];
        // if (seq_pos) {
        //     copy(seq_pos, seq_pos + nb_sequence, new_seq_pos);
        //     delete [] seq_pos;
        // }
        // seq_pos = new_seq_pos;

        while( getline(fichier, ligne) ){

            if( ligne.size() > 0 ){
                //cout << format << endl;
                if( format == 1 ){
                    //fasta
                    if( (ligne.at(0) == '>' || ligne.at(0) == ';') ){

                        
                        //nb_sequence++;
                        //size_t * new_seq_pos = new size_t[nb_sequence];
                        // if (seq_pos) {
                        //     copy(seq_pos, seq_pos + nb_sequence, new_seq_pos);
                        //     delete [] seq_pos;
                        // }
                        // seq_pos = new_seq_pos;
                        //si a la ligne suivante
                        if( (int)fichier.tellg() > (int)ligne.size() ){
                            indice_seq++;
                            size_t num = (size_t)((size_t)fichier.tellg() - (size_t)ligne.size() - (size_t)1);
                            seq_pos[indice_seq - 1] = num;
                            //cout << " pos " << seq_pos[indice_seq - 1];

                            //seq_pos[nb_sequence].seq = (SequenceFasta *) malloc(sizeof(SequenceFasta));
                            //seq_pos[nb_sequence].seq = new SequenceFasta();
                            /*if(seq_pos[nb_sequence].seq == NULL)
                                cout << "Error" << endl;

                            seq_pos[nb_sequence].seq -> setPosition( (int)fichier.tellg() - ligne.size() );*/
                        }
                        

                        //seq_pos[nb_sequence].seq -> setTaille(0);
                        //seq_pos[nb_sequence]. seq -> setEntete(ligne.c_str());
                        //seq_pos[nb_sequence].seq = new SequenceFasta();
                    }

                    else{
                        //seq_pos[nb_sequence - 1].seq -> setTaille( seq_pos[nb_sequence - 1].seq -> getTaille() + ligne.size() + 1 );
                    }
                }
                else{
                    //fastq
                    
                    //nb_sequence++;

                    // size_t *new_seq_pos = new size_t[nb_sequence+1];
                    // if (seq_pos) {
                    //     copy(seq_pos, seq_pos + nb_sequence, new_seq_pos);
                    //     delete [] seq_pos;
                    // }
                    // seq_pos = new_seq_pos;

                    if( ligne.at(0) == '@' && positionFastQ == 0 ){
                        //cout << "er " << ligne.at(0) << endl;
                        if( (int)fichier.tellg() > (int)ligne.size() ){
                            indice_seq++;
                            seq_pos[indice_seq-1] = (size_t)((size_t)fichier.tellg() - (size_t)ligne.size() - (size_t)1);
                            taille_Seq = 0;
                        }
                    }
                    else{
                        if(ligne.at(0) == '+'){
                            positionFastQ = 1;
                            if( getline(fichier, ligne) ){
                                positionFastQ = 2;
                            }
                        }

                        if( positionFastQ == 0 ){
                            taille_Seq += ligneSansSpace(ligne).size();
                        }
                        else if( positionFastQ == 2 ){
                            size_t size = ligne.size();

                            while( size < taille_Seq && getline(fichier, ligne) ){
                                size += ligne.size();
                            }
                            positionFastQ = 0;
                        }
                    }

                }
            }
            num_ligne++;
        }
        fichier.close();  
    }
    else{
        cerr << "Impossible d'ouvrir le fichier !" << file_name << endl;
    }

    return seq_pos;
}

//Destructeur
FichierFastX::~FichierFastX(){

    if( file_name ){
        //cout << "begin delete " << file_name <<  endl;
        delete [] file_name;
        file_name = NULL;
        //cout << "delete file_name " << endl;
    }

    if( seq_pos ){
        //cout << "begin delete 2 " << endl;
        delete [] seq_pos; 
        seq_pos = NULL;
        //cout << "delete seq_pos " << endl;
    }

    if( sequenceFastx ){
        delete sequenceFastx;
        sequenceFastx = NULL;
        //cout << "delete sequenceFastx " << endl;
    }

}
