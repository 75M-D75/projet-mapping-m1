#ifndef DEF_STRUCT_DICO_
#define DEF_STRUCT_DICO_

#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fstream>
#include <vector>
#include <regex>
#include <string.h>
#include <stdlib.h>

#include "EncodedSequence.h"


using namespace std;

class Arbre{

private :

public :

	class Node {
		
	public:
		Node * pere;
		std::vector<Node *> fils;
		char caractere;
		size_t nb_fils;
		size_t num;

		Node(const char c = '\0');
		Node(const Node &n);
		~Node();
		Node &operator=(const Node &n);
		void setPere(Node *p);

		size_t getNbFils();

		/* enleve le noeud f de l'ensemble des fils du noeud courant sans le supprimer */
		void enleveFils(Node &f);

		void ajouteFils(Node &f);
		Node *chercheCaractere(char c) const;
		Node *ajouteCaractere(char c);
		void toStream(std::ostream &os) const;

	};

	
	Node racine;
	size_t num;

	Arbre();

	Node * addChar(char c, Node * node);

	Arbre & addChaine(const string chaine);

	Node * getRacine();

	bool trouve(string mot);

	void toStream(std::ostream &os) const;
};

std::ostream &operator<<(std::ostream &os, const Arbre &a);

void naive(string T, size_t n, char ** P, size_t m);

vector<vector<size_t>>& naive_tab(string T, size_t n, char ** P, const size_t m, vector<vector<size_t>>& tab);

vector<vector<size_t>>& naive_tab_k_mer(string T, string k_mer, string (*pointeurFonction)(string, size_t, size_t), const size_t k, vector<vector<size_t>> &tab);

vector<vector<size_t>>& naive_tab_k_mer(string T, string k_mer, const size_t k, vector<vector<size_t>> &tab);

vector<string> naive_tab_k_mer_s(string T, string k_mer, const size_t k, vector<vector<size_t>> &tab);

vector<string> naive_tab_k_mer_s_last(string T, const string k_mer, const size_t k, vector<vector<size_t>> &tab, size_t &last);

vector<string> naive_tab_k_mer_s_last_enc(EncodedSequence & T, EncodedSequence & k_mer, const size_t k, vector<vector<size_t>> &tab, size_t &last);

vector<string> naive_tab_k_mer_s_last_enc_rev_comp(EncodedSequence & T, EncodedSequence & k_mer, const size_t k, vector<vector<size_t>> &tab, size_t &last);

int rmain();

#endif