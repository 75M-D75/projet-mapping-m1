


//Animation titre
var tabTitre = ["#Lina: <De BCD je serai passée DCD sans cet esprit de solidarité ++>", "#Barry: <The most important thing is not the destination but the journey with my friends>", "#Sanou: <Chaque jour passé en groupe, j'avais l'impréssion que je connaissais rien tellement que j'apprenais à nouveau. Merci la famille", "#Elodie: <Tchimbé red pa moli>", "#Qiqi: Trop trop trop gentille"];
var indiceTitre = 0;

function ecriture(){
    var i=0;
    $(".phrase_equipe").text("|");

    var time = 2000/tabTitre[indiceTitre].length;
    var set = setInterval(interne, time);
    var setA = null;
    var setE = null;

    function interne(){

        if(i < tabTitre[indiceTitre].length){
            var text = $(".phrase_equipe").text().substr(0, i-1) + tabTitre[indiceTitre][i]+"|";
            $(".phrase_equipe").text(text);
            //console.log(titre_texte[i]);
        }
        else{
            clearInterval(set);
            setA = setInterval(attente, 1000);
            i = 0;
        }
        i++;
    }

    var visible = true;

    function attente(){
        if(visible){
            $(".phrase_equipe").text($(".phrase_equipe").text().substr(0, tabTitre[indiceTitre].length-1) + " ");
            visible = false;
        }
        else{
            $(".phrase_equipe").text($(".phrase_equipe").text().substr(0, tabTitre[indiceTitre].length-1)+ "|");
            visible = true;
        }
        i++
        if( i >= 2){
            clearInterval(setA);
            i = 0;
            setE = setInterval(effacer, time);
        }
    }


    function effacer(){

        if(i < tabTitre[indiceTitre].length){
            var text = $(".phrase_equipe").text().substr(0, tabTitre[indiceTitre].length-i-1) + "|";
            $(".phrase_equipe").text(text);
            //console.log(titre_texte[i]);
        }
        else{
            clearInterval(setE);
            i = 0;
            indiceTitre++;

            if( indiceTitre >= tabTitre.length ){
                indiceTitre = 0;
            }
            //time = 2000/tabTitre[indiceTitre].length;
            set = setInterval(interne, time);
        }
            i++;
    }
}
ecriture();


//Effet1
const text = baffle('.data');
text.set({
  characters : 'zpxzVpasdfh86136░█▒ ░░░█▓ >░░ ▓/▒█▓ █ █>█▒sayg▒ ░░░█▓ >yf',
  speed: 150
});
text.start();
text.reveal(70000);

const text1 = baffle('.quote');
text1.set({
  characters : 'qwertyuiopasdfgh8613611888',
  speed: 140
});
text1.start();
text1.reveal(5500);


var derniere_position_de_scroll_connue = 0;
var ticking = false;

function faireQuelqueChose(position_scroll) {
    console.log("scroll "+$(".no_float").offset().left);
    console.log("scrollno "+$(".float").offset().left);
    $(".float").offset().left = $(".no_float").offset().left;
    $(".float").offset({left:$(".no_float").offset().left})
    console.log("scrollafter "+$(".no_float").offset().left);

    if($(".no_float").offset().top < $(".float").offset().top){
        $(".float").css("opacity", 1);
    }
    else{
        $(".float").css("opacity", 0);
    }
}

window.addEventListener('scroll', function(e) {
  derniere_position_de_scroll_connue = window.scrollY;

  if (!ticking) {
    window.requestAnimationFrame(function() {
      faireQuelqueChose(derniere_position_de_scroll_connue);
      ticking = false;
    });
  }

  ticking = true;
});


//Cursor
const cursor = document.querySelector('.cursor');

document.addEventListener('mousemove', e => {
    cursor.setAttribute("style", "top: "+(e.pageY - 10)+"px; left: "+(e.pageX - 10)+"px;")
})

document.addEventListener('click', () => {
    cursor.classList.add("expand");

    setTimeout(() => {
        cursor.classList.remove("expand");
    }, 500)
})