#include "ReadFastX.h"
#include "EncodedSequence.h"


ReadFastX::ReadFastX(string read): read(read) {

}

string ReadFastX::getRead(){
	return read;
}

size_t ReadFastX::getSizeRead(){
	return read.size();
}

string ReadFastX::k_mer(size_t k, size_t pos){
	try{
		if( k > read.size() || pos+k > read.size() ){
			throw "k trop elever ou position trop elever";
		}
	}
	catch( const char * chaine ){
		cout << chaine << endl;
		return "";
	}

	return read.substr(pos, k);
}


string k_mer(string read, size_t k, size_t pos){
	try{
		if( k > read.size() || pos+k > read.size() ){
			throw "k trop elever ou position trop elever";
		}
	}
	catch( const char * chaine ){
		cout << chaine << endl;
		return "";
	}

	return read.substr(pos, k);
}

string k_mer(EncodedSequence read, size_t k, size_t pos){
	try{
		if( k > read.getSize() || pos+k > read.getSize() ){
			throw "k trop elever ou position trop elever";
		}
	}
	catch( const char * chaine ){
		cout << chaine << endl;
		return "";
	}

	return read.subStr(pos, k);
}